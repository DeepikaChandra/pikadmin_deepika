import React, { Component } from 'react';
import Parse from 'parse';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import Login from './components/auth/LoginForm';
import Register from './components/auth/Registration';
import ForgetPassword from './components/auth/forgetPassword';
import ResetPassword from './components/auth/resetPassword';
import VerifyPassword from './components/auth/verifyPassword';
import VerifyEmail from './components/auth/verifyEmail';
import ConfirmEmail from './components/auth/confirmEmail';
import TotalUser from './components/board/TotalUser/TotalUser';
import BusinessUser from './components/board/BusinessUser/BusinessUser';
import PushNotification from './components/board/PushNotification/PushNotification';
import Reports from './components/board/Reports/Reports';
import CompletedOrders from './components/board/Orders/CompletedOrders/CompletedOrders';
import PendingOrders from './components/board/Orders/PendingOrders/PendingOrders';
import MarketingSales from './components/board/MarketingSales/MarketingSales';
import Profile from './components/board/Setting/Profile';
import Reset from './components/board/Setting/Reset';
import Resend from './components/board/Setting/Resend';

class App extends Component {
  constructor(props){
    super(props);
    Parse.serverURL = 'http://46.101.81.115:1337/parse';
    Parse.initialize("Ut8ls4jolah6BeJj", "dXdWwxTpHoM7qK0i");
  }
  
   render() {
    let routes = (
      <Switch>
        <Route path="/register" component={Register} />
        <Route path="/forgetPassword" component={ForgetPassword} />
        <Route path="/verifyPassword" component={VerifyPassword} />
        <Route path="/resetPassword" component={ResetPassword} />
        <Route path="/confirmEmail" component={ConfirmEmail} />
        <Route path="/verifyEmail" component={VerifyEmail} />;
        <Route path="/login" exact component={Login} />
        <Redirect to="/login" />
      </Switch>
    );

    if (localStorage.getItem("token")) {
      routes = (
        <Switch>
          <Route path="/totalUser" component={TotalUser} />
          <Route path="/businessUser" component={BusinessUser} />
          <Route path="/pushNotification" component={PushNotification} />
          <Route path="/reports" component={Reports} />
          <Route path="/completedOrders" component={CompletedOrders} />
          <Route path="/pendingOrders" component={PendingOrders} />
          <Route path="/marketingSales" component={MarketingSales} />
          <Route path="/profile" component={Profile} />
          <Route path="/reset" component={Reset} />
          <Route path="/resend" component={Resend} />
          <Redirect to="/totalUser" />
        </Switch>
      );
    }

     return routes;
  }
}
export default withRouter(App);
