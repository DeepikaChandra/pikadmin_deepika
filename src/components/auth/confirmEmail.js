import React from "react";
import AuthLayout from "../layouts/authLayout";
import { Link } from 'react-router-dom';

const confirmEmail = props =>
  (<AuthLayout>
    <form className="card">
      <div className="p-4 p-sm-5">
        <div className="display-1 lnr lnr-checkmark-circle text-center text-success mb-4" />
        <p className="text-center text-big mb-4">
          Your email address has been successfully confirmed.
            </p>
        <Link to="/"><button
          type="button"
          className="btn btn-primary btn-block">
          Proceed to your account
            </button></Link>
      </div>
    </form>
  </AuthLayout>);

export default confirmEmail;