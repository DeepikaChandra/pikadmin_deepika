import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";
import Form from '../utils/form';
import LoginLayout from '../layouts/loginLayout';

class ForgetPassword extends Component {

  submitHandler = () => {
    const user = { email: this.getEmail.value };
    this.props.onForgetPassword(user);
  }

  render() {
    return (
      <LoginLayout>
        <h4 className="text-center font-weight-normal mt-5 mb-0">Forget password ?</h4>
        <Form onSubmit={this.submitHandler} className='my-5' >
        <p>Enter your email address and we will<br></br>send you a link to reset your password.</p>
          <div className="form-group">
            <label className="form-label">Enter email</label>
            <input type="email"
              name="email"
              id="email"
              required={true}
              ref={(input) => this.getEmail = input}
              className="form-control mb-2"
              placeholder="Enter email" />
            <div className="error small form-text invalid-feedback" />
          </div>
          <button id="sign-in" type="submit" className="btn btn-color btn-block mt-4">Reset password</button>
          {/* </div> */}
        </Form >
        {/* <!-- / Form --> */}
        </LoginLayout>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onForgetPassword: user => dispatch(actions.forgetPassword(user)),
  }
}

export default connect(null, mapDispatchToProps)(ForgetPassword);