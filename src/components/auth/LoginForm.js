import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";
import { Link } from 'react-router-dom';
import Form from '../utils/form';
import LoginLayout from '../layouts/loginLayout';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'password',
      passwordEye: 'fa fa-fw fa-eye'
    }
  }

  submitHandler = () => {
    const email = this.getEmail.value;
    const password = this.getPassword.value;
    this.props.onLogin(email, password);
  }

  passwordToggle = () => this.setState(({type}) => ({
    type: type === 'text' ? 'password' : 'text',
    passwordEye: type === 'text' ? 'fa fa-fw fa-eye': 'fa fa-fw fa-eye-slash'
  }))
  

  render() {
    return (
      <LoginLayout>
        <h4 className="text-center  font-weight-normal mt-5 mb-0">Login to Your Account</h4>
        <Form onSubmit={this.submitHandler} className='my-5' >
          <div className="form-group">
            <label className="form-label">User Name</label>
            <input type="text"
              name="user"
              id="email"
              required={true}
              ref={(input) => this.getEmail = input}
              className="form-control mb-2"
              placeholder="Enter User Name" />
            <div className="error small form-text invalid-feedback" />
          </div>
          <div className="form-group">
            <label className="form-label d-flex justify-content-between align-items-end">
              <div>Password</div>
            </label>
            <input type={this.state.type}
              name="password"
              id="password"
              required={true}
              ref={(input) => this.getPassword = input}
              className="form-control mb-2"
              placeholder="Enter Password" />
            <span class="field-icon" className={`${this.state.passwordEye} field-icon`} onClick={this.passwordToggle}>{this.state.type === 'text' ? '' : ''}</span>
            <Link to="/forgetPassword" className="d-block small btn-text-color text-right" tabIndex="-1">Forgot password?</Link>
            <div className="error small form-text invalid-feedback" />
          </div>
          {/* <div className="d-flex justify-content-between align-items-center m-0"> */}
          {/* <label className="custom-control custom-checkbox m-0">
                        <input type="checkbox" className="custom-control-input" />
                        <span className="custom-control-label">Remember me</span>
                      </label> */}
          <button id="sign-in" type="submit" className="btn btn-color btn-block mt-4">Sign In</button>
          {/* </div> */}
        </Form >
        {/* <!-- / Form --> */}

        < div className="text-center text-muted" >
          Don't have an account yet? <Link to="/register" className="btn-text-color">Sign Up</Link>
        </div >
      </LoginLayout>
    );
  }
}

const mapDispatchForLoginForm = dispatch => {
  return {
    onLogin: (email, password) => dispatch(actions.login(email, password)),
  };
};

export default connect(null, mapDispatchForLoginForm)(LoginForm);