import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";
import Form from "../utils/form";
import { Link } from "react-router-dom";
import LoginLayout from "../layouts/loginLayout";

class Registration extends Component {
  state = {
    user: {},
    passwordType: 'password',
    passwordEye: 'fa fa-fw fa-eye',
    confirmPasswordType: 'password',
    confirmPasswordEye: 'fa fa-fw fa-eye'
  };

  passwordToggle = () => this.setState(({passwordType}) => ({
    passwordType: passwordType === 'text' ? 'password' : 'text',
    passwordEye: passwordType === 'text' ? 'fa fa-fw fa-eye': 'fa fa-fw fa-eye-slash'
  }))

  confirmPasswordToggle = () => this.setState(({confirmPasswordType}) => ({
    confirmPasswordType: confirmPasswordType === 'text' ? 'password' : 'text',
    confirmPasswordEye: confirmPasswordType === 'text' ? 'fa fa-fw fa-eye': 'fa fa-fw fa-eye-slash'
  }))

  inputChangeHandler = event => {
    const user = { ...this.state.user };
    user[event.target.name] = event.target.value;
    if (user.password && user.confirmPassword) {
      if (user.password !== user.confirmPassword) {
        this.setState({ message: "Password doesn't Match" });
      } else {
        this.setState({ message: null });
      }
    }
    this.setState({ user: user });
  };

  submitHandler = () => {
    if (this.state.user.password !== this.state.user.confirmPassword) {
      this.setState({ message: "Password doesn't Match" });
    } else {
      this.setState({ message: null });
      this.props.onRegister(this.state.user);
    }
  };

  render() {
    return (
      <LoginLayout>
        <h4 className="text-center font-weight-normal mt-5 mb-0">
          Create an Account
        </h4>
        <Form onSubmit={this.submitHandler} className="my-5">
          <div className="form-group">
            <label className="form-label">Email*</label>
            <input
              type="Email"
              name="email"
              id="email"
              required={true}
              onChange={this.inputChangeHandler}
              className="form-control mb-2"
              placeholder="Enter User Name"
            />
            <div className="error small form-text invalid-feedback" />
          </div>
          <div className="form-group">
            <label className="form-label">User Name*</label>
            <input
              type="text"
              name="userName"
              id="userName"
              required={true}
              onChange={this.inputChangeHandler}
              className="form-control mb-2"
              placeholder="Enter User Name"
            />
            <div className="error small form-text invalid-feedback" />
          </div>
          <div className="form-group">
            <label className="form-label">Password*</label>
            <input
              type={this.state.passwordType}
              name="password"
              id="password"
              required={true}
              onChange={this.inputChangeHandler}
              className="form-control mb-2"
              placeholder="Password"
            />
            <span class="field-icon" className={`${this.state.passwordEye} field-icon`} onClick={this.passwordToggle}>{{ ...this.state.passwordType } === 'text' ? '' : ''}</span>
            <div className="error small form-text invalid-feedback" />
          </div>
          <div className="form-group">
            <label className="form-label">Confirm Password*</label>
            <input
              type={this.state.confirmPasswordType}
              name="confirmPassword"
              id="confirmPassword"
              required={true}
              onChange={this.inputChangeHandler}
              className="form-control mb-2"
              placeholder="Re-enter Password"
            />
            <span class="field-icon" className={`${this.state.confirmPasswordEye} field-icon`} onClick={this.confirmPasswordToggle}>{{ ...this.state.confirmPasswordType } === 'text' ? '' : ''}</span>
            <span className="error small form-text text-danger">
              {this.state.message}
            </span>
            <div className="error small form-text invalid-feedback" />
          </div>
          <button
            id="register"
            className="btn btn-color btn-block mt-4"
            type="submit"
          >
            Register
          </button>
          {/* <div className="text-light small mt-4">
                By clicking "Sign Up", you agree to our
                <Link href="/"><a>terms of service and privacy policy</a></Link>. We’ll occasionally send you account related emails.
              </div> */}
        </Form>
        {/* <!-- / Form --> */}
        <div className="text-center text-muted">
          Already have an account?{" "}
          <Link to="/" className="btn-text-color">
            Sign In
          </Link>
        </div>
      </LoginLayout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onRegister: user => dispatch(actions.register(user))
  };
};
export default connect(null, mapDispatchToProps)(Registration);
