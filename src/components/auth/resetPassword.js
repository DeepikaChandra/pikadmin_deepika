import React, { Component } from "react";
import AuthLayout from "../layouts/authLayout";
import Form from '../utils/form';
import { connect } from "react-redux";
import * as actions from "../../store/actions/actions";

class ResetPassword extends Component {
  state = {
    data: {},
    token: '',
    isMatch: false,
  }

  componentDidMount() {
    const params = new URLSearchParams(this.props.location.search);
    const token = params.get('id');
    if (!token) {
      this.props.history.push('/forgetPassword');
    }
    this.setState({token: token});
  }

  inputChangeHandler = event => {
    const data = { ...this.state.data };
    data[event.target.name] = event.target.value;
    if (data.password && data.confirmPassword) {
      if (data.password !== data.confirmPassword) {
        this.setState({ message: "Password doesn't Match", isMatch: false });
      } else {
        this.setState({ password: data.password, isMatch: true, message: null });
      }
    }
    this.setState({ data: data });
  }

  submitHandler = () => {
    if (this.state.isMatch) {
      const user = {
        token: this.state.token, password: this.state.password
      }
      this.props.onResetPassword(user);
    }
  }

  render() {
    return (
      <AuthLayout>
        <Form className="card" onSubmit={this.submitHandler}>
          <div className="p-4 p-sm-5">

            {/* <!-- Logo --> */}
            <div className="d-flex justify-content-center align-items-center pb-2 mb-4">
              <div className="ui-w-60">
                <div className="w-100 position-relative" style={{ "paddingBottom": "54%" }}>
                </div>
              </div>
            </div>
            {/* <!-- / Logo --> */}

            <h5 className="text-center text-muted font-weight-normal mb-4">Reset password</h5>
            <hr className="mt-0 mb-4" />
            <p>Enter a new password to reset your old password.</p>
            <div className="form-group">
              <small className="form-text text-muted">
                Must contain atleast 8 characters
              </small>
              <input
                type="password"
                className="form-control"
                name="password"
                minLength="8"
                required={true}
                onChange={this.inputChangeHandler}
                placeholder="New password" />
              <div className="error small form-text invalid-feedback" />
            </div>
            <div className="form-group">
              <input
                type="password"
                className="form-control"
                name="confirmPassword"
                required={true}
                onChange={this.inputChangeHandler}
                placeholder="Re-Enter password" />
              <div className="error small form-text invalid-feedback" />
              <span className="error small form-text text-danger">
                {this.state.message}
              </span>
            </div>
            <button
              type="submit"
              className="btn btn-color btn-block">
              Reset Password
            </button>
          </div>
        </Form>
      </AuthLayout>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onResetPassword: user => dispatch(actions.resetPassword(user)),
  }
}

export default connect(null, mapDispatchToProps)(ResetPassword);