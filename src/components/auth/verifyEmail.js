import React from "react";
import LoginLayout from "../layouts/loginLayout";
import { Link } from 'react-router-dom';

const verifyEmail = props =>
  (<LoginLayout>
    <div className="card">
      <div className="p-4 p-sm-5">
        <div className="display-1 lnr lnr-checkmark-circle text-center text-success mb-4" />
        {/* <h5 className="text-center"> Hi, { props.firstName}</h5> */}
        <p className="text-center text-big">
          You have successfully created <span className="text-info mx-2">Admin Panel</span>Account.</p>
          <Link to="/login" className="d-block btn-text-color">Go Back to Login Page</Link>
        {/* <p className="text-center text-big mb-4">Please verify your email address and complete your registration</p>
        <a href="https://mail.google.com"
              className="btn btn-primary btn-block">
              Verify Your email Account 
            </a> */}
      </div>
    </div>
  </LoginLayout>
  );

export default verifyEmail;