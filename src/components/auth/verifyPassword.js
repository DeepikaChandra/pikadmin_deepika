import React, { Component } from "react";
import LoginLayout from '../layouts/loginLayout';

class verifyPassword extends Component
{
  render() {
    return (
  <LoginLayout>
    <div className="card">
      <div className="p-4 p-sm-5">
        <div className="display-1 lnr lnr-checkmark-circle text-center text-success mb-4" />
        <p className="text-center text-big mb-4">
            A link to reset your password has been send to your email account.
        </p>
        {/* <button className="btn btn-primary btn-block">Go To email</button> */}
      </div>
    </div>
  </LoginLayout>
      );
    }
}

export default verifyPassword;