import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import "react-table/react-table.css";
import "./PushNotification.css";
import Parse from "parse";
import swal from "sweetalert";

class PushNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title_left: 40,
      message_left: 90
    };
  }

  // for counting the number of words typing by user
  handleWordCount = event => {
    const id = event.target.id;
    const charCount = event.target.value.length;
    if (id === "titleMessage") {
      const title_left = 40 - charCount;
      this.setState({
        title_left: title_left
      });
    }
    if (id === "alertMessage") {
      const message_left = 90 - charCount;
      this.setState({
        message_left: message_left
      });
    }
  };

  async PushNotification() {
    var users = document.getElementById("userType").value;
    if (users === "" || (users === "") === undefined) {
      swal(
        "Warning",
        "Please select the user to push the notification",
        "warning"
      );
      return;
    }
    let titleMessage = document.getElementById("titleMessage").value;
    let alertMessage = document.getElementById("alertMessage").value;
    if (titleMessage === "" || titleMessage === undefined) {
      swal("Warning", "Please Enter the Title", "warning");
      return;
    }
    if (alertMessage === "" || alertMessage === undefined) {
      swal("Warning", "Please Enter the Message", "warning");
      return;
    }
    const params = {
      channelType:  users,
      titleMessage: titleMessage,
      alertMessage: alertMessage
      // AllBusiness for all business
      //single user user_lRHEXiTicY
      //all customer AllCustomer
    };
    await Parse.Cloud.run("userpush", params);
    titleMessage = document.getElementById("titleMessage").value = "";
    alertMessage = document.getElementById("alertMessage").value = "";
  }

  render() {
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            {/*
			<div className="row">
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div classNames="btn-group" id="status" data-toggle="buttons">
						<label className="btn btn-default btn-on active" id="allUser">
							<input type="radio" value="1" className="hideRadio" checked />Users</label>
						<label className="btn btn-default btn-off" id="businessUser">
							<input type="radio" value="0" className="hideRadio" />Business</label>
					</div>
				</div>
			</div>
			<br />*/}
            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <label>
                  <select id="userType" className="userType">
                    <option value="" id="">
                      ----Select User----
                    </option>
                    <option value="AllCustomer" id="uae">
                      AllCustomer
                    </option>
                    <option value="AllBusiness" id="us">
                      AllBusiness
                    </option>
                    <option value="SACustomer" id="uae">
                      SACustomer
                    </option>
                    <option value="SABusiness" id="ksa">
                      SABusiness
                    </option>
                    <option value="AECustomer" id="us">
                      AECustomer
                    </option>
                    <option value="AEBusiness" id="ksa">
                      AEBusiness
                    </option>
                    <option value="USCustomer" id="us">
                      USCustomer
                    </option>
                    <option value="USBusiness" id="ksa">
                      USBusiness
                    </option>
                  </select>
                </label>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label>TITLE</label>
                <br />
                <input
                  type="text"
                  className="input-text input-text-a"
                  id="titleMessage"
                  maxLength="40"
                  required
                  onChange={this.handleWordCount}
                />
                {this.state.title_left}/40
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <label>MESSAGE</label>
                <br />
                <textarea
                  type="text"
                  className="input-text input-text-b"
                  id="alertMessage"
                  maxLength="90"
                  required
                  onChange={this.handleWordCount}
                />
                {this.state.message_left}/90
              </div>
            </div>
            <div>
              <button
                className="buttonStyle btn-color"
                onClick={() => this.PushNotification()}
              >
                SEND
              </button>
            </div>
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default PushNotification;
