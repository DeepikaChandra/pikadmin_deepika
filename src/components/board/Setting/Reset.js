import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import Parse from "parse";
import log from 'loglevel';
import swal from 'sweetalert'
import history from '../../../store/history';

class ResetPassword extends Component {

  resetPassword() {
    let email = this.email.value;
    if (email === "") {
        swal("Warning", "Enter email", "warning")
        return;
    }
    Parse.User.requestPasswordReset(email)
        .then(() => {
            history.push('/resend');
        }).catch((error) => {
            log.error(error.message);
        });
};
  render() {
    return (
      <MainLayout>
      <main role="main" className=" flex-grow-2  container-p-y">
        <div className="container">
          <div className="form-group">
            <div className="row">
              <div className="col-md-4 col-lg-4">
                <label className="form-label">Email</label>
                <input type="email" name="email" id="email" required={true} ref={el=>this.email = el} className="form-control mb-2" placeholder="Enter email" />
                <div className="error small form-text invalid-feedback" /></div>
              <div className="col-md-8 col-lg-8"></div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 col-lg-4">
              <button id="sign-in" type="submit" className="btn btn-color btn-block mt-4" onClick={()=>this.resetPassword()}>Reset password</button>
            </div>
            <div className="col-md-8 col-lg-8"></div>
          </div>
        </div>
      </main>
    </MainLayout>
    );
  }
}

export default ResetPassword;
