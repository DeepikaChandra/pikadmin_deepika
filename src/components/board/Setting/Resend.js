import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import history from '../../../store/history';

class ResetPassword extends Component {
    resendEmail(){
        history.push('/reset');
    }
  render() {
    return (
        <MainLayout>
        <div className="p-4 p-sm-5">
            <div className="display-1 lnr lnr-checkmark-circle text-center text-success mb-4" />
            <p className="text-center text-big mb-4">Sent Succesfully</p>
            <div className="row">
                <div className="col-log-4 col-md-4"></div>
                <div className="col-log-4 col-md-4">
                    <button id="done" type="submit" className="btn btn-color btn-block mt-4" onClick={()=>this.resendEmail()}>Resend Email</button>
                </div>
                <div className="col-log-4 col-md-4"></div>
            </div>
        </div>
    </MainLayout>
    );
  }
}

export default ResetPassword;
