import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import swal from "sweetalert";
import Parse from "parse";
import log from "loglevel";

class Profile extends Component {
  updateProfile = () => {
    let fullName = this.fullName.value;
    let email = this.email.value;
    if (fullName === "") {
      swal("Warning", "Enter full Name", "warning");
      return;
    }
    if (email === "") {
      swal("Warning", "Enter Email", "warning");
      return;
    }
    var user = Parse.User.current();
    user.fetch().then(
      function(fetchedUser) {
        let currentUser = fetchedUser.id;
        var userObject = Parse.User.current();
        userObject.set("objectId", currentUser);
        userObject.save().then(userObject => {
          userObject.set("name", fullName);
          userObject.set("email", email);
          swal("Great", "Profile Updated Succesfully", "success");
          return userObject.save();
        });
      },
      function(error) {
        log.error(error.message);
      }
    );
  };

  render() {
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-lg-4">
                <label>Full Name</label>
                <br />
                <input
                  type="text"
                  name="firstName"
                  id="firstName"
                  required={true}
                  className="form-control mb-2"
                  placeholder="Enter Full Name"
                  ref={el => (this.fullName = el)}
                />
              </div>
              <div className="col-md-8 col-lg-8"></div>
            </div>
            <div className="row">
              <div className="col-md-4 col-lg-4">
                <label>Email</label>
                <br />
                <input
                  type="email"
                  name="user"
                  id="email"
                  required={true}
                  className="form-control mb-2"
                  placeholder="Enter Email"
                  ref={el => (this.email = el)}
                />
              </div>
              <div className="col-md-8 col-lg-8"></div>
            </div>
            <div className="row">
              <div className="col-md-4 col-lg-4">
                <button
                  id="done"
                  type="submit"
                  className="btn btn-color btn-block mt-4"
                  onClick={() => this.updateProfile()}
                >
                  Done
                </button>
              </div>
              <div className="col-md-8 col-lg-8"></div>
            </div>
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default Profile;
