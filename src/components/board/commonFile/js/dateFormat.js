export const dateFormat = (event) => {
    var date = new Date(event),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join("/");
}

export const fromDate = (event) =>{
    let pieces = event.split("-")
    let fromDate = pieces[0]
    return fromDate;
}

export const toDate = (event) => {
    var pieces = event.split("- ")
    var toDate = pieces[1]
   return toDate;
}

export const dateValidate = (date) =>{
    var reverseDate = date.split("/").reverse().join("/");
    var dateFormat = new Date(reverseDate);
    return dateFormat;
}

export const dateTimeFormat = (date) => {
    var d = date;
    let dformat = [d.getDate(),
        d.getMonth()+1,
        d.getFullYear()].join('/')+' '+
       [d.getHours(),
        d.getMinutes(),
        d.getSeconds()].join(':');
        return dformat;
}