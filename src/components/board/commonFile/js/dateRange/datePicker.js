import React from 'react';
import DatetimeRangePicker from "../dateRange/index";
import moment from 'moment';
import "../../css/datePicker.css";

class PredefinedRanges extends React.Component {

  constructor(props) {
    super(props);

    this.handleApply = this.handleApply.bind(this);

    this.state = {
      startDate: moment().subtract(14, "days"),
      endDate: moment(),
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      },
    };
  }

  handleApply(event, picker) {
    this.setState({
      startDate: picker.startDate,
      endDate: picker.endDate
    });
  }

  render() {
    let start = this.state.startDate.format('DD/MM/YYYY hh:mm a');
    let end = this.state.endDate.format('DD/MM/YYYY hh:mm a');
    let label = start + ' - ' + end;
    if (start === end) {
      let todayStart = this.state.startDate.format('DD/MM/YYYY')
      todayStart = todayStart+" 12:00 am";
      let todayEnd = this.state.startDate.format('DD/MM/YYYY')
      todayEnd = todayEnd+" 11:59 pm";
      label = todayStart + ' - ' + todayEnd;
    }

    let locale = {
      format: 'DD/MM/YYYY HH:mm:ss',
      separator: ' - ',
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
      weekLabel: 'W',
      customRangeLabel: 'Custom Range',
      daysOfWeek: moment.weekdaysMin(),
      monthNames: moment.monthsShort(),
      firstDay: moment.localeData().firstDayOfWeek(),
    };

    return (
      <div>
          <DatetimeRangePicker
            timePicker
            timePicker24Hour
            showDropdowns
            timePickerSeconds
            locale={locale}
            ranges={this.state.ranges}
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            onApply={this.handleApply}
          >
            <input
            type="text"
            size="35"
            id="dateRange"
            value={label}
            className="customSelect"
            style={{border:'1px solid #ccc'}}
          />
          </DatetimeRangePicker>
      </div>
    );
  }

}

export default PredefinedRanges;