import swal from 'sweetalert'

export const exportCsv = (data) => {
  const csvRow = [];
  const columnName = [['S.no', 'Date', 'Business Name', 'Location Name','Sub Total', 'Total Order', 'Total sales', 'Discount Pik', 'Discount Business', 'Refund orders','Refund Amount','Phone Number','Promo Code']];
  if(data.length === 0){
    swal("Warning", "No Data Found", "warning")
    return;
  }
  for(let item =0; item<data.length; item++)
  {var myStr = data[item].locationName;
   var locationName = myStr.replace(/,/g, '-');
    columnName.push([data[item].Sno, data[item].orderDate, data[item].name, locationName, 
      data[item].originalSalesTotal, data[item].totalOrders, data[item].totalSales, data[item].pikDiscount,data[item].businessDiscount, data[item].totalRefunds, data[item].refundAmount, data[item].phoneNumber, data[item].promoCode]);
  }
  for(let i=0; i<columnName.length; ++i){
    csvRow.push(columnName[i].join(","))
  }
  const csvString  = csvRow.join("%0A");
  const clickDownload = document.createElement("a");
  clickDownload.href = 'data:attachment/csv,'+csvString;
  clickDownload.terget="_blank";
  clickDownload.download = "joinpik.csv";
  document.body.appendChild(clickDownload);
  swal("Success", "your file downloaded Successfully", "success")
  clickDownload.click();
}