import swal from "sweetalert";
import * as date from "../commonFile/js/dateFormat";
import pikURL from "./../../../assets/images/CoffeePik.jpg"

export const exportPDFDataTable = data => {
  if (data.length === 0) {
    swal("Warning", "No data found...!", "warning");
    return;
  }
  var mywindow = window.open("", "Print", "height=600,width=800");
  mywindow.document.write(
    '<html><head><title>Print</title><link media="all" />'
  );
  mywindow.document.write("</head><body>");
  mywindow.document.write('<h2 align="center"><i>Pik Admin Panel</i></h2>');
  mywindow.document.write('<table border="1" width="100%"><head>');
  mywindow.document.write(
    "<tr><th>S.no</th><th>Date</th><th>Business Name</th><th>Location Name</th><th>Total Order</th>" +
      "<th>Total sales</th><th>Discount Business</th><th>Refund orders</th><th>Refund Amount</th><th>Phone Number</th><th>Promo Code</th></tr></head><tbody>"
  );
  for (let i = 0; i < data.length; i++) {
    let Sno = data[i].Sno;
    let orderDate = data[i].orderDate;
    let businessName = data[i].businessName;
    let locationName = data[i].locationName;
    let totalOrdersCount = data[i].totalOrdersCount;
    let totalSales = data[i].totalSales;
    let discountBusiness = data[i].discountBusiness;
    let totalRefunds = data[i].totalRefunds;
    let refundAmount = data[i].refundAmount;
    let phoneNumber = data[i].phoneNumber;
    let promoCode = data[i].promoCode;

    mywindow.document.write(
      '<tr align="center"><td>' +
        Sno +
        "</td><td>" +
        orderDate +
        "</td><td>" +
        businessName +
        "</td><td>" +
        locationName +
        "</td>" +
        "<td>" +
        totalOrdersCount +
        "</td><td>" +
        totalSales +
        "</td><td>" +
        discountBusiness +
        "</td><td>" +
        totalRefunds +
        "</td><td>" +
        refundAmount +
        "</td><td>" +
        phoneNumber +
        "</td><td>" +
        promoCode +
        "</td></tr>"
    );
  }
  mywindow.document.write("</tbody></table></body></html>");
  mywindow.document.close();
  mywindow.focus();
  mywindow.print();
  window.close();
  return true;
};

// ------------------------------------------ Generate PDF for Orders -------------------------------------------------------

export const exportPDFOrders = data => {
  if (data.length === 0) {
    swal("Warning", "No data found...!", "warning");
    return;
  }
  let totalOrdersCount = 0;
  let OriginalSales = 0;
  let refundOrders = 0;
  let totalCommissionFee = 0;
  let pikDiscount = 0;
  let businessDiscount = 0;
  let accountBalance = 0;
  let balanceDue = 0;
  let bankCharges = 0;
  let refundCharges = 0;
  let totalAmount = 0;
  let businessName;
  let beforeTotal=0;

  for (let i = 0; i < data.length; i++) {
    totalOrdersCount += data[i].totalOrdersCount;
    OriginalSales += data[i].originalSales;
    refundOrders += data[i].refundOrders;
    totalCommissionFee += data[i].totalCommissionFee;
    pikDiscount += data[i].pikDiscount;
    businessDiscount += data[i].businessDiscount;
    accountBalance += data[i].accountBalance;
    balanceDue = data[i].balanceDue;
    bankCharges = data[i].bankCharges;
    refundCharges += data[i].refundCharges;
    businessName = data[i].businessName;
  }
  beforeTotal = balanceDue + refundCharges + accountBalance;
  totalAmount=beforeTotal;
  if (beforeTotal > 500) {
    totalAmount =beforeTotal - bankCharges;
  }
  const dateRange = document.getElementById("dateRange").value;
  let fromDate = date.fromDate(dateRange);
  let toDate = date.toDate(dateRange);
  let piecesFromDate = fromDate.split(" ")
  let fromDateSelected = piecesFromDate[0]
  let piecesToDate = toDate.split(" ")
  let toDateSelected = piecesToDate[0]
  var mywindow = window.open("", "Print", "height=600,width=800");
  mywindow.document.write(
    '<html><head><title>Print</title><link media="all" />'
  );
  mywindow.document.write("</head><body>");
  mywindow.document.write(
    '<label align="left"><img src='+pikURL+' style="height:"20px"; width:"20px"; padding:"2px;""></label>'
  );
  mywindow.document.write(
    '<h2 align="left"><b style="color:#673ab7;">Account Statement</b></h2>'
  );
  mywindow.document.write(
    '<p style="border-bottom: 2.5px solid black; font-size:3vw;" align="left">From: ' +
    fromDateSelected +
      " To " +
      toDateSelected +
      "</p>"
  );
  mywindow.document.write(
    '<p style="border-bottom: 2.5px solid black">Account Statement for <b>' +
      businessName +
      "</b> </p>"
  );
  mywindow.document.write(
    '<table width="100%"><thead style="border - bottom: 1px solid black">'
  );
  mywindow.document.write(
    "<tr><th align='left'>Total Orders</th><th align='left'>Original Sales</th><th align='left'>Refund Orders</th>" +
      "<th align='left'>Commission Fee</th><th align='left'>Pik Discount</th><th align='left'>Business Discount</th><th align='left'>Account Balance</th></tr></thead><tbody>"
  );
  mywindow.document.write("<tr>");
  mywindow.document.write("<td align='left'>" + totalOrdersCount + "</td>");
  mywindow.document.write(
    "<td align='left'>" + OriginalSales.toFixed(2) + "</td>"
  );
  mywindow.document.write(
    "<td align='left'>" + refundOrders + "</td>"
  );
  mywindow.document.write(
    "<td align='left'>" + totalCommissionFee.toFixed(2) + "</td>"
  );
  mywindow.document.write(
    "<td align='left'>" + pikDiscount.toFixed(2) + "</td>"
  );
  mywindow.document.write(
    "<td align='left'>" + businessDiscount.toFixed(2) + "</td>"
  );
  mywindow.document.write(
    "<td align='left'>" + accountBalance.toFixed(2) + "</td>"
  );
  mywindow.document.write("</tr>");
  mywindow.document.write("</tbody></tfoot>");
  mywindow.document.write("<hr style='border: 1px solid black' />");
  mywindow.document.write(
    "<tr align='left'><th>" +
    totalOrdersCount +
      "</th><th>" +
      OriginalSales.toFixed(2) +
      "</th><th>" +
      refundOrders +
      "</th>" +
      "<th>" +
      totalCommissionFee.toFixed(2) +
      "</th><th>" +
      pikDiscount.toFixed(2) +
      "</th><th>" +
      businessDiscount.toFixed(2) +
      "</th>" +
      "<th>" +
      accountBalance.toFixed(2) +
      "</th></tr></tfoot></table>"
  );
  mywindow.document.write('<table width="100%">');
  mywindow.document.write("<hr style='border: 1px solid black' />");
  mywindow.document.write("<br /><br />");
  mywindow.document.write(
    '<tr><td >Balance Due</td><td  align="right">' +
      balanceDue.toFixed(2) +
      "</td></tr>"
  );
  mywindow.document.write(
    '<tbody><tr><td align="left"> Refund Charges</td > <td align="right">' +
      refundCharges.toFixed(2) +
      "</td></tr>"
  );
  if(beforeTotal>500){
  mywindow.document.write(
    '<tr><td align="left">Bank Transfer Charges</td><td align="right">' +
      "-"+bankCharges.toFixed(2) +
      "</td></tr>"
  );
  }else {
    mywindow.document.write(
      '<tr><td align="left">Bank Transfer Charges</td><td align="right">' +
        0.00 +
        "</td></tr>"
    );
  }
  mywindow.document.write(
    '<tr><td align="left">Payout this month</td><td align="right">' +
      accountBalance.toFixed(2) +
      "</td></tr>"
  );
  mywindow.document.write("</tbody></table>");

  
  mywindow.document.write('<table width="100%">');
  mywindow.document.write("<br /><br />");
  mywindow.document.write(
    '<tr ><td align="left"><b>total</b></td><td align="right"><b>' +
      totalAmount.toFixed(2) +
      "</b></td></tr>"
  );
  mywindow.document.write("</table>");
  mywindow.document.write("</body></html>");
  mywindow.document.close();
  mywindow.focus();
  setTimeout(function(){   mywindow.print();  }, 500);
  window.close();
  return true;
};
