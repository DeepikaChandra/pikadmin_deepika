import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import "./Reports.css";
import log from "loglevel";
import Parse from "parse";
import _ from "lodash";
import ReactTable from "react-table";
import CountUp from "react-countup";
import "../commonFile/css/reactTable.scss";
import * as Excel from "./excel";
import * as Pdf from "./pdf";
import * as date from "../commonFile/js/dateFormat";
import PredefinedRanges from "../commonFile/js/dateRange/datePicker";
import { Multiselect } from "multiselect-react-dropdown";
import { Loader } from 'react-overlay-loader';

class Reports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalTransactions: "",
            totalDiscount: "",
            currency: "",
            grossSales: "",
            netSales: "",
            pikCharges: "",
            notesValue: "",
            data: [],
            shopLocations: [],
            locationList: "",
            selectedLocation: [],
            loading: true
        };
        this.transaction = this.transaction.bind(this);
    }
    
    componentDidMount() {
        this._isMounted = true;
        this.onload();
    }
    
    componentWillUnmount() {
        this._isMounted = false;
    }
    
    onload() {
        this.setState({
            loading: true
        })
        let countryCode = document.getElementById("country").value;
        const dateRange = document.getElementById("dateRange").value;
        let fromDate = date.fromDate(dateRange);
        let toDate = date.toDate(dateRange);
        const country = Parse.Object.extend("Country");
        let countryQuery = new Parse.Query(country);
        countryQuery.equalTo("shortName", countryCode);
        let countryObj = [];
        countryQuery.find().then(
            result => {
                for (let i = 0; i < result.length; i++) {
                    countryObj.push(result[i].id);
                }
                this.transaction(countryObj, fromDate, toDate);
                this.DataTable(countryObj, fromDate, toDate);
            },
            error => {
                log.error(error.message);
            }
        );
    }
    
    transaction(countryObj, fromDate, toDate) {
        let totalTransactions = 0;
        let totalDiscount = 0;
        let grossSales = 0;
        let netSales = 0;
        let currency = "";
        let pikCharges = 0;
        let orderStatus ='';
        const orderHistory = Parse.Object.extend("OrderHistory");
        let orderHistoryObj = new Parse.Query(orderHistory);
        let pointers = _.map(countryObj, function(objectId) {
            const pointer = new Parse.Object("Country");
            pointer.id = objectId;
            return pointer;
        });
        // Query for Platform
        let platform = document.getElementById("platform").value;
        if (
            platform !== "" &&
            platform === "PF"
        ) {
            orderHistoryObj.equalTo("userPlatform", platform);
        } else if (
            platform !== "" &&
            platform === "CP"
        ) {
            orderHistoryObj.containedIn("userPlatform", [platform, undefined]);
        } else if (
            platform !== "" &&
            platform === "BN"
        ) {
            orderHistoryObj.equalTo("userPlatform", platform);
        }
        let countryCode = document.getElementById("country").value;
        if (countryCode === "AE") {
            orderHistoryObj.containedIn("country", [pointers, undefined]);
        } else {
            orderHistoryObj.containedIn("country", pointers);
        }
        orderHistoryObj.greaterThanOrEqualTo(
            "orderTime",
            date.dateValidate(fromDate)
        ); //date filter req as on load is running
        orderHistoryObj.lessThanOrEqualTo("orderTime", date.dateValidate(toDate));
          // Business wise filter the data
          let businessName = document.getElementById("searchForBusiness").value;
          if (businessName !== undefined && businessName !== "") {
              const users = Parse.Object.extend("User");
              let innerUsersQuery = new Parse.Query(users);
              innerUsersQuery.matches("Business_name", businessName);
              orderHistoryObj.matchesQuery("owner", innerUsersQuery);
              // locationWise filter the Data
              let locationList = this.state.selectedLocation;
              if (locationList !== null && locationList.length > 0) {
                  let ShopObject = Parse.Object.extend("ShopLocations");
                  let innerShopQuery = new Parse.Query(ShopObject);
                  innerShopQuery.containedIn("locationName", locationList);
                  orderHistoryObj.matchesQuery("shop", innerShopQuery);
              }
          }
          let inputValue = this.inputValue.value;
          const notes = document.getElementById("notes").value;
          orderHistoryObj.include("shop");
          orderHistoryObj.include("owner");
          orderHistoryObj.include("offerDetails");
          orderHistoryObj.limit(1000000);
          let ShopLocations = Parse.Object.extend("ShopLocations");
          let shopInnerQuery = new Parse.Query(ShopLocations);
      
          let UserObject = Parse.Object.extend("User");
          let innerUserQuery = new Parse.Query(UserObject);
          let offerObject = Parse.Object.extend("Offers");
          let offerInnerQuery = new Parse.Query(offerObject);
          if (notes !== "" && notes !== undefined && notes === "Business_name") {
              innerUserQuery.matches("Business_name", inputValue);
              orderHistoryObj.matchesQuery("owner", innerUserQuery);
          } else if (
              notes !== "" &&
              notes !== undefined &&
              notes === "Phone_Number"
          ) {
              shopInnerQuery.equalTo("phoneNo", Number(inputValue));
              orderHistoryObj.matchesQuery("shop", shopInnerQuery);
          } else if (
              notes !== "" &&
              notes !== undefined &&
              notes === "Discount_Paid_By"
          ) {
              orderHistoryObj.matches("discountPaidBy", inputValue);
          } else if (notes !== "" && notes !== undefined && notes === "Promo_Code") {
              offerInnerQuery.matches("offerCode", inputValue);
              orderHistoryObj.matchesQuery("offerDetails", offerInnerQuery);
          }
          orderHistoryObj.ascending("orderTime");
          orderHistoryObj.exists("owner");
          orderHistoryObj.exists("shop");
        orderHistoryObj.find().then(
            result => {
                for (let i = 0; i < result.length; i++) {
                    totalTransactions = result.length;
                    if (
                        result[i].get("orderStatus") !== undefined &&
                        result[i].get("orderStatus") !== ""
                    ) {
                        orderStatus = result[i].get("orderStatus");
                    }
                    if (
                        result[i].get("discountAmount") !== undefined &&
                        result[i].get("discountAmount") !== ""
                    ) {
                        totalDiscount += parseFloat(result[i].get("discountAmount"));
                    }
                    if (
                        result[i].get("currency") !== undefined &&
                        result[i].get("currency") !== ""
                    ) {
                        currency = result[i].get("currency");
                    } else {
                        currency = "";
                    }
                    if (
                        result[i].get("subTotal") !== undefined &&
                        result[i].get("subTotal") !== ""  &&
                        orderStatus !== 4
                    ) {
                        grossSales += parseFloat(result[i].get("subTotal"));
                    }
                    if (
                        result[i].get("pikCharges") !== undefined &&
                        result[i].get("pikCharges") !== ""
                    ) {
                        pikCharges += parseFloat(result[i].get("pikCharges"));
                    }
                    if (
                        result[i].get("totalCost") !== undefined &&
                        result[i].get("totalCost") !== ""
                    ) {
                        netSales += parseFloat(result[i].get("totalCost"));
                    }
                }
    
                this.setState({
                    totalTransactions: totalTransactions,
                    totalDiscount: totalDiscount,
                    currency: currency,
                    grossSales: grossSales,
                    netSales: netSales,
                    pikCharges: pikCharges
                });
            },
            error => {
                log.error(error.message);
            }
        );
    }
    
    DataTable(countryObj, fromDate, toDate) {
        const orderHistory = Parse.Object.extend("OrderHistory");
        let orderHistoryObj = new Parse.Query(orderHistory);
        let pointers = _.map(countryObj, function(objectId) {
            const pointer = new Parse.Object("Country");
            pointer.id = objectId;
            return pointer;
        });
        let countryCode = document.getElementById("country").value;
        if (countryCode === "AE") {
            orderHistoryObj.containedIn("country", [pointers, undefined]);
        } else {
            orderHistoryObj.containedIn("country", pointers);
        }
        orderHistoryObj.greaterThanOrEqualTo(
            "orderTime",
            date.dateValidate(fromDate)
        );
        orderHistoryObj.lessThanOrEqualTo("orderTime", date.dateValidate(toDate));
        // Query for Platform
        let platform = document.getElementById("platform").value;
        if (
            platform !== "" &&
            platform === "PF"
        ) {
            orderHistoryObj.equalTo("userPlatform", platform);
        } else if (
            platform !== "" &&
            platform === "CP"
        ) {
            orderHistoryObj.containedIn("userPlatform", [platform, undefined]);
        } else if (
            platform !== "" &&
            platform === "BN"
        ) {
            orderHistoryObj.equalTo("userPlatform", platform);
        }
        // Business wise filter the data
        let businessName = document.getElementById("searchForBusiness").value;
        if (businessName !== undefined && businessName !== "") {
            const users = Parse.Object.extend("User");
            let innerUsersQuery = new Parse.Query(users);
            innerUsersQuery.matches("Business_name", businessName);
            orderHistoryObj.matchesQuery("owner", innerUsersQuery);
            // locationWise filter the Data
            let locationList = this.state.selectedLocation;
            if (locationList !== null && locationList.length > 0) {
                let ShopObject = Parse.Object.extend("ShopLocations");
                let innerShopQuery = new Parse.Query(ShopObject);
                innerShopQuery.containedIn("locationName", locationList);
                orderHistoryObj.matchesQuery("shop", innerShopQuery);
            }
        }
        let inputValue = this.inputValue.value;
        const notes = document.getElementById("notes").value;
        orderHistoryObj.include("shop");
        orderHistoryObj.include("owner");
        orderHistoryObj.include("offerDetails");
        orderHistoryObj.limit(1000000);
        let ShopLocations = Parse.Object.extend("ShopLocations");
        let shopInnerQuery = new Parse.Query(ShopLocations);
    
        let UserObject = Parse.Object.extend("User");
        let innerUserQuery = new Parse.Query(UserObject);
        let offerObject = Parse.Object.extend("Offers");
        let offerInnerQuery = new Parse.Query(offerObject);
        if (notes !== "" && notes !== undefined && notes === "Business_name") {
            innerUserQuery.matches("Business_name", inputValue);
            orderHistoryObj.matchesQuery("owner", innerUserQuery);
        } else if (
            notes !== "" &&
            notes !== undefined &&
            notes === "Phone_Number"
        ) {
            shopInnerQuery.equalTo("phoneNo", Number(inputValue));
            orderHistoryObj.matchesQuery("shop", shopInnerQuery);
        } else if (
            notes !== "" &&
            notes !== undefined &&
            notes === "Discount_Paid_By"
        ) {
            orderHistoryObj.matches("discountPaidBy", inputValue);
        } else if (notes !== "" && notes !== undefined && notes === "Promo_Code") {
            offerInnerQuery.matches("offerCode", inputValue);
            orderHistoryObj.matchesQuery("offerDetails", offerInnerQuery);
        }
        let sno = 0;
        orderHistoryObj.ascending("orderTime");
        orderHistoryObj.exists("owner");
        orderHistoryObj.exists("shop");
        let dataList = [];
        orderHistoryObj.find().then(
            result => {
                if(result.length>0){
                let arrayList = [];
                for (let i = 0; i < result.length; i++) {
                    let cancelledBy = "";
                    let offerCode = "";
                    let gateway = "";
                    let originalSales = 0;
                    let refundOrders = 0;
                    let pikDiscount = 0;
                    let businessDiscount = 0;
                    let owner;
                    let waived='';
                    let accountBalance = 0;
                    let balanceDue = 0;
                    let bankCharges = 0;
                    let subTotal = 0;
                    let refundCharges = 0;
                    let totalAmount = 0;
                    let discountGiven = "";
                    let totalSalesSum = 0;
                    let refundAmountSum = 0;
                    let orderDate;
                    let business_name = "";
                    let locationName;
                    let currentDate;
                    let offerPointer;
                    let ownerPointer;
                    let phoneNumber;
                    let refundPikCharges = 0;
                    let totalCommissionPercentage=0;
                    let shopPointer = "";
                    let businessPointer = "";
                    let shopPointerLoopId;
                    let shopPointerId;
                    let shopPointerLoop;
                    let dateAndLocPointer;
                    let totalOrdersCount = 0;
                    let totalRefundsCount = 0;
                    let totalCommissionFee=0;
                    let originalSalesTotal = 0;
                    
                    if (result[i].get('orderTime') !== undefined && result[i].get('orderTime') !== "") { // current date and order date
                        orderDate = date.dateFormat(result[i].get('orderTime'));
                        currentDate = date.dateFormat(result[i].get('orderTime'));
                    } else {
                        orderDate = "";
                        currentDate = "";
                    }
                    if (result[i].get("shop") !== undefined && result[i].get("shop") !== "") {
                        shopPointer = result[i].get("shop"); // Location name
                    }
                    shopPointerId = shopPointer.id;
                    dateAndLocPointer = currentDate + shopPointerId;
                    if (arrayList.includes(dateAndLocPointer)) {
                        continue;
                    } else {
                        arrayList.push(dateAndLocPointer);
                        if (result[i].get("owner") !== undefined && result[i].get("owner") !== "") {
                            businessPointer = result[i].get("owner"); // Business name
                            if (
                                businessPointer.get("Business_name") !== undefined ||
                                businessPointer.get("Business_name") !== ""
                            ) {
                                business_name = businessPointer.get("Business_name");
                            } else {
                                business_name = "";
                            }
                        }
                        if (result[i].get("shop") !== undefined && result[i].get("shop") !== "") {
                            shopPointer = result[i].get("shop"); // Location name
                            if (shopPointer.get("locationName") !== undefined || shopPointer.get("locationName") !== '') {
                                locationName = shopPointer.get("locationName");
                            } else {
                                locationName = "No Shop";
                            }
                        } else {
                            locationName = "No Shop";
                        }
    
                        if (
                            result[i].get("owner") !== undefined &&
                            result[i].get("owner") !== ""
                        ) {
                            ownerPointer = result[i].get("owner");
                            if (
                                ownerPointer.get("phoneNumber") !== undefined &&
                                ownerPointer.get("phoneNumber") !== ""
                            ) {
                                phoneNumber = ownerPointer.get("phoneNumber");
                            }
                        }
                        for (let k = i; k < result.length; k++) {
    
                            if (result[k].get("shop") !== undefined && result[k].get("shop") !== "") {
                                shopPointerLoop = result[k].get("shop"); // Location name
                                shopPointerLoopId = shopPointerLoop.id;
                            }
    
                            if (result[k].get('orderTime') !== undefined && result[k].get('orderTime') !== "") { // next date
                                orderDate = date.dateFormat(result[k].get('orderTime'));
                            }
                            if (currentDate === orderDate && shopPointerId === shopPointerLoopId) {
                                let refundNegativeCharges = 0;
                                let refundPositiveCharges = 0;
                                let cancelledByCurrent = "";
                                let refundPikChargesCurrent = 0;
                                let subTotal=0;
                                let orderStatus='';
                                let commissionPercentage = 0;
                                let commissionFee = 0;
                                let refundTime='';
                                totalOrdersCount += 1; 
                                let ownerPointerCurrent='';
                                if (
                                    result[k].get("cancelledBy") !== undefined &&
                                    result[k].get("cancelledBy") !== ""
                                ) {
                                    cancelledBy += result[k].get("cancelledBy") + ", ";
                                    cancelledByCurrent = result[k].get("cancelledBy");
                                }
                                if (
                                    result[k].get("refundCost") !== undefined &&
                                    result[k].get("refundCost") !== ""
                                ) {
                                    refundPikCharges += Number(result[k].get("refundCost"));
                                    refundPikChargesCurrent = Number(result[k].get("refundCost"));
                                }
                                if (
                                    result[k].get("orderStatus") !== undefined &&
                                    result[k].get("orderStatus") !== ""
                                ) {
                                    orderStatus = result[k].get("orderStatus");
                                }
                              
                                if (
                                    result[k].get("subTotal") !== undefined &&
                                    result[k].get("subTotal") !== "" &&
                                    orderStatus !== 4
                                ) {
                                    subTotal=Number(result[k].get("subTotal"));
                                    originalSales += Number(result[k].get("subTotal"));
                                    originalSalesTotal += Number(result[k].get("subTotal"));
                                } else  originalSales += 0;

                                if (
                                    result[k].get("owner") !== undefined &&
                                    result[k].get("owner") !== ""
                                ) {
                                    ownerPointerCurrent = result[k].get("owner");

                                if (
                                    ownerPointerCurrent.get("pikPercentage") !== undefined &&
                                    ownerPointerCurrent.get("pikPercentage") !== ""
                                ) {
                                    totalCommissionPercentage += Number(ownerPointerCurrent.get("pikPercentage"));
                                    commissionPercentage = Number(ownerPointerCurrent.get("pikPercentage"));
                                    commissionFee= (subTotal * commissionPercentage)/100;
                                    totalCommissionFee += commissionFee;
                                }
                            }
                                if (
                                    result[k].get("gateway") !== undefined &&
                                    result[k].get("gateway") !== ""
                                ) {
                                    gateway += result[k].get("gateway") + ", ";
                                }

                               
                                if (orderStatus === 4) {
                                    totalRefundsCount += 1;
                                } else totalRefundsCount += 0;
                                if (
                                    result[k].get("totalCost") !== undefined &&
                                    result[k].get("totalCost") !== ""
                                ) {
                                    // Total sales
                                    totalSalesSum += Number(result[k].get("totalCost"));
                                }
                                if (
                                    result[k].get("discountPaidBy") !== undefined &&
                                    result[k].get("discountPaidBy") !== ""
                                ) {
                                    discountGiven += result[k].get("discountPaidBy") + ", ";
                                }
    
                                if (
                                    result[k].get("refundCost") !== undefined &&
                                    result[k].get("refundCost") !== ""
                                ) {
    
                                    refundAmountSum += Number(result[k].get("refundCost"));
                                }
                                if (
                                    result[k].get("offerDetails") !== undefined &&
                                    result[k].get("offerDetails") !== ""
                                ) {
                                    offerPointer = result[k].get("offerDetails");
                                    if (
                                        offerPointer.get("offerCode") !== undefined &&
                                        offerPointer.get("offerCode") !== ""
                                    ) {
                                        offerCode += offerPointer.get("offerCode") + ", ";
                                    }
                                }
                                // here writing the code for generating the PDF for orders
    
                               
    
                                if (orderStatus === 4) {
                                    refundOrders += 1;
                                }
    
                                if (
                                    result[k].get("discountAmount") !== undefined &&
                                    result[k].get("discountAmount") !== "" &&
                                    orderStatus !== 4
                                ) {
                                    if (result[k].get("discountPaidBy") === "pik") {
                                        pikDiscount += Number(result[k].get("discountAmount"));
                                    }
                                    if (result[k].get("discountPaidBy") === "business") {
                                        businessDiscount += Number(result[k].get("discountAmount"));
                                    }
                                    if(result[k].get("discountPaidBy") === undefined || result[k].get("discountPaidBy") === ""){
                                        pikDiscount += Number(result[k].get("discountAmount"));
                                    }
                                } 
    
                                if (
                                    result[k].get("refundTime") !== undefined &&
                                    result[k].get("refundTime") !== ""
                                ) {
                                    refundTime = result[k].get("refundTime");
                                }
                                if (
                                    cancelledByCurrent === "business" &&
                                    orderStatus === 4 &&
                                    refundPikChargesCurrent !== ""
                                ) {
                                    refundNegativeCharges = refundPikChargesCurrent;
                                }
                                if (
                                    result[k].get("subTotal") !== undefined &&
                                    result[k].get("subTotal") !== "" &&
                                    orderStatus === 4 &&
                                    cancelledByCurrent === "user" &&
                                    refundTime === 1
                                ) {
                                    subTotal = result[k].get("subTotal");
                                    refundPositiveCharges = (subTotal*10)/ 100;
                                }
                                refundCharges += refundPositiveCharges - refundNegativeCharges;
                               
                            } else if (currentDate !== orderDate) {
                                break;
                            }
                        }
                        if (result[i].get("owner") !== undefined && result[i].get("owner") !== "") {
                            owner = result[i].get("owner");
                            if (owner.get("waived") !== "") {
                                waived = Number(owner.get("waived"));
                            }
                            if (
                                owner.get("balanceDue") !== undefined &&
                                owner.get("balanceDue") !== ""
                            ) {
                                balanceDue = Number(owner.get("balanceDue"));
                            }
                            if (
                                owner.get("bankCharges") !== undefined &&
                                owner.get("bankCharges") !== ""
                            ) {
                                bankCharges = Number(owner.get("bankCharges"));
                            }
                        }
                        if (waived < 1 || isNaN(waived)) {
                            accountBalance =
                                originalSales - totalCommissionFee - businessDiscount;
                        } else {
                            accountBalance = originalSales - businessDiscount;
                        }
                    }
                    if (offerCode === "") {
                        offerCode = "N/A";
                    }
                    if (gateway === "") {
                        gateway = "N/A";
                    }
                    if (cancelledBy === "") {
                        cancelledBy = "N/A";
                    }
                    if (refundPikCharges === "") {
                        refundPikCharges = "N/A";
                    }
                    if (totalRefundsCount === 0) {
                        refundAmountSum = "N/A";
                    }
                    if (totalSalesSum === "") {
                        totalSalesSum = "N/A";
                    }
                    if (totalCommissionPercentage === "") {
                        totalCommissionPercentage = "N/A";
                    }
                    if (discountGiven === "") {
                        discountGiven = "N/A";
                    }
                    sno = sno + 1;
                    const dataValue = {
                        Sno: sno,
                        orderDate: currentDate,
                        name: business_name,
                        locationName: locationName,
                        totalOrders: totalOrdersCount,
                        originalSalesTotal:originalSalesTotal,
                        totalSales: totalSalesSum,
                        discountGiven: discountGiven,
                        totalRefunds: totalRefundsCount,
                        refundAmount: refundAmountSum,
                        promoCode: offerCode,
                        phoneNumber: phoneNumber,
                        gateway: gateway,
                        cancelledBy: cancelledBy,
                        refundPikCharges: refundPikCharges,
                        totalCommissionPercentage: totalCommissionPercentage,
                        commissionFee: totalCommissionFee,
                        // here adding the data for genrating pdf for orders
                        totalOrdersCount:totalOrdersCount,
                        businessName:business_name,
                        originalSales: originalSales,
                        refundOrders: refundOrders,
                        totalCommissionFee: totalCommissionFee,
                        pikDiscount: pikDiscount,
                        businessDiscount: businessDiscount,
                        accountBalance: accountBalance,
                        balanceDue: balanceDue,
                        bankCharges: bankCharges,
                        refundCharges: refundCharges,
                        totalAmount: totalAmount
                    };
    
                    dataList.push(dataValue);
                }
            }else{
                    this.checkingNoResults(businessName);
                  }
            this.setState({
                data: dataList,
                loading: false
            });
              
            },
            error => {
                log.error(error.message);
            }
        );
    }
    checkingNoResults(businessName){
        const users = Parse.Object.extend("User");
        let usersQuery = new Parse.Query(users);
        usersQuery.equalTo("Business_name", businessName);
        let dataList=[];
        usersQuery.find().then(
            result => {
                for (let i = 0; i < result.length; i++) {
                    let balanceDue=0;
                    let totalAmount=0;
                    if (
                        result[i].get("balanceDue") !== undefined &&
                        result[i].get("balanceDue") !== ""
                    ) {
                        balanceDue = Number(result[i].get("balanceDue"));
                    }
                    totalAmount=balanceDue;
                    const dataValue = {
                        totalOrdersCount:0,
                        businessName:businessName,
                       originalSales: 0,
                       refundOrders: 0,
                        totalCommissionFee: 0,
                        pikDiscount: 0,
                       businessDiscount: 0,
                       accountBalance: 0,
                       balanceDue: balanceDue,
                       bankCharges: 0,
                        refundCharges: 0,
                        totalAmount: totalAmount
                    };
                    dataList.push(dataValue);
                }
                this.setState({
                     data:dataList
                });
            },
            error => {
              log.error(error.message);
            }
          );
    }

    searchByBusiness() {
        const users = Parse.Object.extend("User");
        let usersQuery = new Parse.Query(users);
        let searchForBusiness = document.getElementById("searchForBusiness").value;
        if (searchForBusiness === "") {
            this.setState({
                selectedLocation: [],
                shopLocations: []
            });
            this.onload();
        }
        usersQuery.exists("Business_name");
        usersQuery.equalTo("Business_name", searchForBusiness);
        let businessObjId = [];
        usersQuery.find().then(
            result => {
                for (let i = 0; i < result.length; i++) {
                    businessObjId.push(result[i].id);
                }
                // query for filter by date and country
                let countryCode = document.getElementById("country").value;
                const dateRange = document.getElementById("dateRange").value;
                let fromDate = date.fromDate(dateRange);
                let toDate = date.toDate(dateRange);
                const country = Parse.Object.extend("Country");
                let countryQuery = new Parse.Query(country);
                countryQuery.equalTo("shortName", countryCode);
                let countryObj = [];
                countryQuery.find().then(result => {
                    for (let i = 0; i < result.length; i++) {
                        countryObj.push(result[i].id);
                    }
                    this.DataTable(countryObj, fromDate, toDate);
                    this.filterByLocation(businessObjId);
                });
            },
            error => {
                log.error(error.message);
            }
        );
    }
    
    filterByLocation(businessObjId) {
        const shop = Parse.Object.extend("ShopLocations");
        let shopLocationsQuery = new Parse.Query(shop);
        shopLocationsQuery.containedIn("business", businessObjId);
        let shopLocations = [];
        shopLocationsQuery.find().then(
            result => {
                for (let i = 0; i < result.length; i++) {
                    const dataValue = {
                        id: result[i].get("locationName"),
                        label: result[i].get("locationName")
                    };
                    shopLocations.push(dataValue);
                }
                this.setState({
                    shopLocations: shopLocations
                });
            },
            error => {
                log.error(error.message);
            }
        );
    }
    resetAll() {
       window.location.reload()
      }

    search() {
        let countryCode = document.getElementById("country").value;
        const country = Parse.Object.extend("Country");
        let countryQuery = new Parse.Query(country);
        countryQuery.equalTo("shortName", countryCode);
        let countryObj = [];
        countryQuery.find().then(
            result => {
                for (let i = 0; i < result.length; i++) {
                    countryObj.push(result[i].id);
                }
                this.onload();
            },
            error => {
                log.error(error.message);
            }
        );
    }
    
    reset() {
        document.getElementById("notes").value = "";
        this.inputValue.value = "";
        document.getElementById("filterByNotes").disabled = true;
        this.onload();
    }
    
    filterNotes(e) {
        document.getElementById("filterByNotes").disabled = false;
    }

    onSelect(e){
        let selectedLocation = [];
        for(let i in e){
            selectedLocation.push(e[i].id)
        } 
        this.setState({
            selectedLocation: selectedLocation
        })
    }
    
    onRemove(e){
      let selectedLocation = [];
      for(let i in e){
          selectedLocation.push(e[i].id)
      } 
      this.setState({
          selectedLocation: selectedLocation
      })
    }

    render() {
            const columns = [{
                    Header: "S.no",
                    accessor: "Sno"
                },
                {
                    Header: "Date",
                    accessor: "orderDate"
                },
                {
                    Header: "Business Name",
                    accessor: "name"
                },
                {
                    Header: "Location Name",
                    accessor: "locationName"
                },
                {
                    Header: "Total Order",
                    accessor: "totalOrders"
                },
                {
                    Header: "Sub Total",
                    accessor: "originalSalesTotal"
                },
                {
                    Header: "Total sales",
                    accessor: "totalSales"
                },
                {
                    Header: "Discount Given",
                    accessor: "discountGiven"
                },
                {
                    Header: "Refund orders",
                    accessor: "totalRefunds"
                },
                {
                    Header: "Refund Amount",
                    accessor: "refundAmount"
                },
                {
                    Header: "Phone Number",
                    accessor: "phoneNumber"
                },
                {
                    Header: "Promo Code",
                    accessor: "promoCode"
                },
                {
                    Header: "Commission Fee",
                    accessor: "commissionFee"
                },
                {
                    Header: "Commission %",
                    accessor: "totalCommissionPercentage"
                },
                {
                    Header: "Refund Pik Charges",
                    accessor: "refundPikCharges"
                },
                {
                    Header: "cancelled By",
                    accessor: "cancelledBy"
                },
                {
                    Header: "Payment Getways",
                    accessor: "gateway"
                }
            ];
    const { loading } = this.state;
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            <div className="row">
                <div className="col-lg-4">
                    <PredefinedRanges id="dateRange" />
                </div>
                <div className="col-4">
                    <select
                        className="filter-note"
                        id="notes"
                        onChange={e => this.filterNotes(e)}
                    >
                        <option value="">Notes</option>
                        <option value="Business_name" id="Business name">
                            Business Name
                        </option>
                        <option value="Phone_Number" id="Phone Number">
                            Phone Number
                        </option>
                        <option value="Discount_Paid_By" id="Discount Business">
                            Discount Paid By
                        </option>
                        <option value="Promo_Code" id="Promo Code">
                            Promo Code
                        </option>
                    </select>
                    <input
                    type="text"
                    id="filterByNotes"
                    className="filter-note-text"
                    placeholder="&nbsp; Filter by notes"
                    ref={el => (this.inputValue = el)}
                    disabled
                    />
                    <input
                    type="text"
                    id="input2"
                    size="1"
                    className="filter-note-iconBox notes-text-icon pointer"
                    onClick={() => this.reset()}
                    />
                </div>
                <div className="col-2">
                    <select
                    className="custom-select"
                    id="platform"
                    >
                    <option value="" hidden>Platform</option>
                    <option value="PF" id="PF">
                        Pik Food
                    </option>
                    <option value="CP" id="CP">
                        Coffee Pik
                    </option>
                    <option value="BN" id="BN">
                        Beans
                    </option>
                    <option value="" id="">
                        All (Platform)
                    </option>
                    </select>
                </div>
                <div className="col-2">
                    <button
                    className="dropdown-toggle button-export"
                    data-toggle="dropdown"
                    >
                    Export<span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu dropdown-menu-right">
                    <li>
                        <a
                        href="#1"
                        id="notes"
                        className="pointer"
                        onClick={() => Excel.exportCsv(this.state.data)}
                        >
                        Microsoft Excel (.xlsx)
                        </a>
                    </li>
                    <li>
                        <a
                        href="#1"
                        id="notes"
                        className="pointer"
                        onClick={() => Pdf.exportPDFDataTable(this.state.data)}
                        >
                        PDF Document (.pdf)
                        </a>
                    </li>
                    <li>
                        <a
                        href="#pdf"
                        id="notes"
                        className="pointer"
                        onClick={() => Pdf.exportPDFOrders(this.state.data)}
                        >
                        PDF Document Table Data (.pdf)
                        </a>
                    </li>
                    </ul>
                </div>
            </div><br />
            <div className="row">
                <div className="col-lg-4">
                    <input className="search__input" id="searchForBusiness" onKeyUp={(e) => this.searchByBusiness(e)} placeholder="search" />
                </div>
                <div className="col-lg-4">
                    <Multiselect
                    options={this.state.shopLocations}
                    displayValue="id"
                    onSelect={(e)=>this.onSelect(e)}
                    onRemove={(e)=>this.onRemove(e)}
                    placeholder="Select Location"
                    showCheckbox={false}
                    avoidHighlightFirstOption={true}
                    closeOnSelect={false}
                    style={{ chips: { background: "#673ab7" }, multiselectContainer: {color: "#673ab7"}}}
                    />
                </div>
                <div className="col-2">
                <button
                    type="button"
                    className="searchBtn"
                    onClick={() => this.search()}
                >
                  Search
                </button>
                </div>
                <div className="col-2">
                    <button
                    type="button"
                    className="searchBtn"
                    onClick={() => this.resetAll()}
                    >
                    Reset
                    </button>
                </div>
            </div>
            <br />
            <div className="row">
              <div className="col-lg-2">
                <h4>
                  <CountUp end={Number(this.state.totalTransactions)} />
                </h4>
                <p>TRANSACTION</p>
              </div>
              <div className="col-lg-2">
                <h4>
                  <CountUp
                    decimals={2}
                    end={Number(this.state.totalDiscount)}
                  />
                  {this.state.currency}
                </h4>
                <p>DISCOUNT</p>
              </div>
              <div className="col-lg-3">
                <h4>
                  <CountUp decimals={2} end={Number(this.state.grossSales)} />
                  {this.state.currency}
                </h4>
                <p>GROSS SALES</p>
              </div>
              <div className="col-lg-3">
                <h4>
                  <CountUp decimals={2} end={Number(this.state.netSales)} />
                  {this.state.currency}
                </h4>
                <p>NET SALES</p>
              </div>
              <div className="col-lg-2">
                <h4>
                  <CountUp decimals={2} end={Number(this.state.pikCharges)} />
                  {this.state.currency}
                </h4>
                <p>PIK CHARGES</p>
              </div>
            </div>
            <br />
            <div>
              <br />
              <Loader loading={loading} />
              <ReactTable
                data={this.state.data}
                columns={columns}
                defaultPageSize={15}
                pageSizeOptions={[3, 5, 15, 50, 100]}
              />
            </div>
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default Reports;
