import swal from 'sweetalert'

export const exportPDF = (data) => {
    if (data.length === 0) {
        swal("Warning", "No data found...! Please import the data by changing the date", "warning")
        return;
    }
    var mywindow = window.open('', 'Print', 'height=600,width=800');
    mywindow.document.write('<html><head><title>Print</title><link media="all" />');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<h2 align="center"><i>Pik Admin Panel</i></h2>');
    mywindow.document.write('<table border="1" width="100%"><head>');
    mywindow.document.write('<tr><th></th><th>object ID</th><th>Brand Name</th><th>Phone Number</th><th>Joined</th>' +
        '<th>Location</th></tr></head><tbody>')
    for (let i = 0; i < data.length; i++) {
        let Sno = data[i].Sno;
        let objectId = data[i].objectId;
        let brandName = data[i].brandName;
        let PhoneNumber = data[i].phoneNumber;
        let joined = data[i].joined;
        let location = data[i].location;
        mywindow.document.write(
            '<tr><td>' + Sno + '</td><td>' + objectId + '</td><td>' + brandName + '</td><td>' + PhoneNumber + '</td>' +
            '<td>' + joined + '</td><td>' + location + '</td></tr>'
        );
    }
    mywindow.document.write('</tbody></table></body></html>');
    mywindow.document.close();
    mywindow.focus()
    mywindow.print();
    window.close();
    return true;
}