import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import ReactTable from "react-table";
import Parse from "parse";
import _ from "lodash";
import CountUp from "react-countup";
import log from "sweetalert";
import PredefinedRanges from "../commonFile/js/dateRange/datePicker";
import * as date from "../commonFile/js/dateFormat";
import * as Excel from "./excel";
import * as Pdf from "./pdf";
import "./BusinessUser.css";
import swal from "sweetalert";
import { Loader } from 'react-overlay-loader';

class BusinessUser extends Component {
  state = {
    totalOrders: "",
    totalBrands: "",
    numberOfShop: "",
    pendingVerify: "",
    location:"",
    popUpData: {
      objectId: "",
      brandName: "",
      phoneNumber: "",
      tax: "",
      taxId: "",
      pik: "",
      accountStatus: "",
      waivedAmount: "",
      balanceDue: "",
      bankCharges: "",
      loading: true
    },
    openModal: false
  };

  componentDidMount() {
    this._isMounted = true;
    this.onload();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onload() {
    this.setState({
      loading: true
    })
    var countryCode = document.getElementById("country").value;
    const dateRange = document.getElementById("dateRange").value;
    var fromDate = date.fromDate(dateRange);
    var toDate = date.toDate(dateRange);
    const country = Parse.Object.extend("Country");
    var countryQuery = new Parse.Query(country);
    countryQuery.equalTo("shortName", countryCode);
    let countryObj = [];
    countryQuery.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          countryObj.push(result[i].id);
        }
        let pointers = _.map(countryObj, function(objectId) {
          const pointer = new Parse.Object("Country");
          pointer.id = objectId;
          return pointer;
        });
        this.totalOrders(pointers, fromDate, toDate);
        this.totalBrands(pointers, fromDate, toDate);
        this.numberOfShop(pointers, fromDate, toDate);
        this.pendingVerify(pointers, fromDate, toDate);
        this.DataTable(pointers, fromDate, toDate);
      },
      error => {
        log.error(error.message);
      }
    );
  }

   DataTable(pointers, fromDate, toDate) {
    let inputValue = this.inputValue.value;
    const notes = document.getElementById("notes").value;
    const account = document.getElementById("account").value;
    const student = document.getElementById("student").value;
    const users = Parse.Object.extend("User");
    let usersObj = new Parse.Query(users);
    usersObj.containedIn("country", pointers);
    usersObj.containedIn("accountStatus", [0, 1, 4]);
    usersObj.greaterThanOrEqualTo("createdAt", date.dateValidate(fromDate));
    usersObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    usersObj.limit(1000000);
    // filter based on notes
    if (notes !== "" && notes !== undefined && notes === "objectId") {
      usersObj.matches("objectId", inputValue);
    } else if (
      notes !== "" &&
      notes !== undefined &&
      notes === "Business_name"
    ) {
      usersObj.matches("Business_name", inputValue);
    } else if (notes !== "" && notes !== undefined && notes === "phoneNumber") {
      usersObj.equalTo("phoneNumber", Number(inputValue));
    } else if (notes !== "" && notes !== undefined && notes === "email") {
      usersObj.equalTo("emailStr", inputValue);
    }
    // filter based on account
    if (account !== "" && account !== undefined && account === "activated") {
      usersObj.equalTo("accountStatus", 1);
    } else if (account !== "" && account !== undefined && account === "deActivated") {
      usersObj.equalTo("accountStatus", 4);
    } else if (account !== "" && account !== undefined && account === "pending") {
      usersObj.equalTo("accountStatus", 0);
    }
    // filter based on student
    if (student !== "" && student !== undefined && student === "activated") {
      usersObj.containedIn("badges", ["EDmnOaCScx", "OjOKNRqDgB"]);
    } else if (student !== "" && student !== undefined && student === "deActivated") {
      usersObj.containedIn("badges", [undefined, ""]);
    }
   usersObj.find().then(
     async result => {
        let dataList = [];
        for (let i = 0; i < result.length; i++) {
          const object = result[i];
          let objectId;
          let brandName;
          let phoneNumber;
          let email;
          let joined;
          let ownerLicense;
          let taxId;
          let taxPercentage = 0;
          let pikPercentage;
          let accountStatus;
          let studentSupport;
          let waivedAmount = 0;
          let balanceDue = 0;
          let bankCharges = 0;
          let shopLicense;
          let bankAdded;
          let noOfLocations=0;

          if (object.id !== undefined && object.id !== "") {
            objectId = object.id;
          } else {
            objectId = "";
          }
          await this.countNoOfLocations(objectId);
          noOfLocations = this.state.location;
          if (
            object.get("Business_name") !== undefined &&
            object.get("Business_name") !== ""
          ) {
            brandName = object.get("Business_name");
          } else {
            brandName = "";
          }
          if (
            object.get("phoneNumber") !== undefined &&
            object.get("phoneNumber") !== ""
          ) {
            phoneNumber = object.get("phoneNumber");
          } else {
            phoneNumber = "";
          }
          if (
            object.get("emailStr") !== undefined &&
            object.get("emailStr") !== ""
          ) {
            email = object.get("emailStr");
          } else {
            email = "";
          }
          if (
            object.get("createdAt") !== undefined &&
            object.get("createdAt") !== ""
          ) {
            joined = date.dateFormat(object.get("createdAt"));
          } else {
            joined = "";
          }
          if (
            object.get("bankAdded") !== undefined &&
            object.get("bankAdded") !== ""
          ) {
            bankAdded = object.get("bankAdded");
          } else {
            bankAdded = '';
          }
          if (
            object.get("ShopLicense") !== undefined &&
            object.get("ShopLicense") !== null &&
            object.get("ShopLicense") !== "" 
          ) {
            shopLicense = object.get("ShopLicense").url();
          } else {
            shopLicense = "";
          }
          if (
            object.get("ownerLicense") !== undefined &&
            object.get("ownerLicense") !== null &&
            object.get("ownerLicense") !== "" 
          ) {
            ownerLicense = object.get("ownerLicense").url();
          } else {
            ownerLicense = "";
          }
          if (object.get("taxId") !== undefined && object.get("taxId") !== "") {
            taxId = object.get("taxId");
          } else {
            taxId = "";
          }
          if (object.get("tax") !== undefined && object.get("tax") !== "") {
            taxPercentage = Number(object.get("tax"));
          } else {
            taxPercentage = "";
          }
          if (
            object.get("pikPercentage") !== undefined &&
            object.get("pikPercentage") !== ""
          ) {
            pikPercentage = Number(object.get("pikPercentage"));
          } else {
            pikPercentage = "";
          }
          if (
            object.get("accountStatus") === 0 ||
            object.get("accountStatus") === undefined ||
            object.get("accountStatus") === ""
          ) {
            accountStatus = "Pending";
          } else if (object.get("accountStatus") === 1) {
            accountStatus = "Active";
          } else if (object.get("accountStatus") === 4) {
            accountStatus = "Deactive";
          }
          if (
            object.get("badges") !== undefined &&
            object.get("badges") !== ""
          ) {
            studentSupport = object.get("badges")[0];
            if(studentSupport === "EDmnOaCScx" || studentSupport === "OjOKNRqDgB"){
              studentSupport = "Activated"
            }
          } else {
            studentSupport = "Deactivated";
          }
          if (
            object.get("waived") !== undefined &&
            object.get("waived") !== ""
          ) {
            waivedAmount = Number(object.get("waived"));
          } else {
            waivedAmount = "";
          }
          if (
            object.get("balanceDue") !== undefined &&
            object.get("balanceDue") !== ""
          ) {
            balanceDue = Number(object.get("balanceDue"));
          } else {
            balanceDue = "";
          }
          if (
            object.get("bankCharges") !== undefined &&
            object.get("bankCharges") !== ""
          ) {
            bankCharges = Number(object.get("bankCharges"));
          } else {
            bankCharges = "";
          }

          const dataValue = {
            Sno: i + 1,
            objectId: objectId,
            brandName: brandName,
            phoneNumber: phoneNumber,
            email: email,
            joined: joined,
            bankAdded: bankAdded,
            noOfLocations: noOfLocations,
            ownerLicense: ownerLicense,
            taxId: taxId,
            shopLicense: shopLicense,
            taxPercentage: taxPercentage,
            pikPercentage: pikPercentage,
            accountStatus: accountStatus,
            studentSupport: studentSupport,
            waivedAmount: waivedAmount,
            balanceDue: balanceDue,
            bankCharges: bankCharges
          };
          dataList.push(dataValue);
        }
        this.setState({
          data: dataList,
          loading: false
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }
     async countNoOfLocations(objectId){
    let count = [];
    const shopLocation = Parse.Object.extend("ShopLocations");
    var shopLocationObj = new Parse.Query(shopLocation);
    shopLocationObj.containedIn("business", [objectId]);
    count = await  shopLocationObj.count();
        this.setState({ 
          location: count
        });
  }


  totalBrands(pointers, fromDate, toDate) {
    let count = 0;
    const user = Parse.Object.extend("User");
    var userObj = new Parse.Query(user);
    userObj.containedIn("country", pointers);
    userObj.greaterThanOrEqualTo(
      "createdAt",
      date.dateValidate(fromDate)
    );
    userObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    userObj.limit(1000000);
    userObj.equalTo("accountStatus", 1);
    userObj.limit(1000000);
    userObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          count = result.length;
        }
        this.setState({
          totalBrands: count
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  totalOrders(pointers, fromDate, toDate) {
    let count = 0;
    const orderHistory = Parse.Object.extend("OrderHistory");
    var orderHistoryObj = new Parse.Query(orderHistory);
    orderHistoryObj.containedIn("country", pointers);
    orderHistoryObj.greaterThanOrEqualTo(
      "createdAt",
      date.dateValidate(fromDate)
    );
    orderHistoryObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    orderHistoryObj.limit(1000000);
    orderHistoryObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          count = result.length;
        }
        this.setState({
          totalOrders: count
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  numberOfShop(pointers, fromDate, toDate) {
    let count = 0;
    const shopLocation = Parse.Object.extend("ShopLocations");
    var shopLocationObj = new Parse.Query(shopLocation);
    shopLocationObj.containedIn("country", pointers);
    shopLocationObj.greaterThanOrEqualTo(
      "createdAt",
      date.dateValidate(fromDate)
    );
    shopLocationObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    shopLocationObj.limit(1000000);
    shopLocationObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          count = result.length;
        }
        this.setState({
          numberOfShop: count
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  pendingVerify(pointers, fromDate, toDate) {
    let count = 0;
    const users = Parse.Object.extend("User");
    let usersObj = new Parse.Query(users);
    usersObj.containedIn("country", pointers);
    usersObj.equalTo("accountStatus", 0);
    usersObj.greaterThanOrEqualTo("createdAt", date.dateValidate(fromDate));
    usersObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    usersObj.limit(1000000);
    usersObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          count = i + 1;
        }
        this.setState({
          pendingVerify: count
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  // for making the textfield edit after selecting the notes
  filterNotes(e) {
    document.getElementById("filterByNotes").disabled = false;
  }

  // click search button for filter
  search() {
    this.onload();
  }

  // clear the notes filter
  reset() {
    document.getElementById("notes").value = "";
    this.inputValue.value = "";
    document.getElementById("filterByNotes").disabled = true;
    this.onload();
  }
  
  resetAll(){
    window.location.reload();
  }

  // for making the text field editable for edit
  onChangeHandle = e => {
    const value = e.target.value;
    this.setState({
      popUpData: {
        [e.target.name]: value
      }
    });
  };

  // for editing the table row
  editRow(e, row) {
    this.setState({
      popUpData: {
        objectId: row.objectId,
        brandName: row.brandName,
        phoneNumber: row.phoneNumber,
        tax: row.taxPercentage,
        taxId: row.taxId,
        pik: row.pikPercentage,
        accountStatus: row.accountStatus,
        waivedAmount: row.waivedAmount,
        balanceDue: row.balanceDue,
        bankCharges: row.bankCharges
      }
    });
  }

  // for updating the data after edit
  async saveRow() {
    this.setState({
      loading: true
    })
    let objectId = document.getElementById("popObjectId").value;
    let accountStatus = document.getElementById("popAccountStatus").value;
    if (accountStatus === "Active") {
      accountStatus = 1;
    } else if (accountStatus === "Pending") {
      accountStatus = 0;
    } else if (accountStatus === "Deactivate") {
      accountStatus = 4;
    }
    let brandName = this.brandName.value;
    let phoneNumber = Number(this.phoneNumber.value);
    let taxPercentage = Number(this.taxPercentage.value);
    let taxId = this.taxId.value;
    let pikPercentage = Number(this.pikPercentage.value);
    let waivedAmount = Number(this.waivedAmount.value);
    let balanceDue = Number(this.balanceDue.value);
    let bankCharges = Number(this.bankCharges.value);
    const params = {
      userId: objectId,
      business_name: brandName,
      phoneNumber: phoneNumber,
      tax: taxPercentage,
      taxId: taxId,
      pikPercentage: pikPercentage,
      accountStatus: accountStatus,
      waived: waivedAmount,
      balanceDue: balanceDue,
      bankCharges: bankCharges
    };
    await Parse.Cloud.run("userUpdate", params);
    this.setState({
      loading: false
    })
    swal("Data Updated Successfully");
  }

  // for viewing the shop License
  viewLicense(e, row) {
    if (row.shopLicense === "") {
      swal("No Record Found");
    } else {
      window.open(row.shopLicense, "_blank");
    }
  }
  // for viewing the owner License
  viewOwnerLicense(e, row) {
      if (row.shopLicense === "") {
        swal("No Record Found");
      } else {
        window.open(row.ownerLicense, "_blank");
      }
    }

  render() {
    const columns = [
      {
        Header: "",
        accessor: "Sno"
      },
      {
        Header: "object ID",
        accessor: "objectId"
      },
      {
        Header: "Brand Name",
        accessor: "brandName"
      },
      {
        Header: "Phone Number",
        accessor: "phoneNumber"
      },{
        Header: "Email",
        accessor: "email"
      },
      {
        Header: "Joined",
        accessor: "joined"
      },
      {
        Header: "Location",
        accessor: "noOfLocations"
      },
      {
        Header: "Shop License",
        accessor: "shopLicense",
        Cell: ({ row }) => (
          <button id="editRow" onClick={e => this.viewLicense(e, row)}>
            View
          </button>
        )
      },
      {
        Header: "Owner License",
        accessor: "ownerLicense",
        Cell: ({ row }) => (
          <button id="editRow" onClick={e => this.viewOwnerLicense(e, row)}>
            View
          </button>
        )
      },
      {
        Header: "Account Status",
        accessor: "accountStatus"
      },
      {
        Header: "Tax %",
        accessor: "taxPercentage"
      },
      {
        Header: "Tax ID",
        accessor: "taxId"
      },
      {
        Header: "Pik %",
        accessor: "pikPercentage"
      },
      {
        Header: "Student Support",
        accessor: "studentSupport"
      },
      {
        Header: "Waived Amount",
        accessor: "waivedAmount"
      },
      {
        Header: "Balace Due",
        accessor: "balanceDue"
      },
      {
        Header: "Bank Added",
        accessor: "bankAdded"
      },
      {
        Header: "Bank Charges",
        accessor: "bankCharges"
      },
      {
        Header: "Edit",
        accessor: "edit",
        Cell: ({ row }) => (
          <button
            className="editButton"
            id="editRow"
            data-toggle="modal"
            data-target="#editModal"
            onClick={e => this.editRow(e, row)}
          >
            edit
          </button>
        )
      }
    ];
    const { loading } = this.state;
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            {/* Pop Up Start Here */}
            <div className="modal fade" id="editModal" tabIndex="-1" role="dialog" aria-labelledby="openMenuTitle" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered modal-items" role="document">
                  <div className="modal-content">
                      <div className="modal-header" style={{backgroundColor: "#673ab7" }}>
                          <h4 className="modal-title" style={{color: "#ffffff"}}>Edit Details</h4>
                          <button type="button" className="closePopUp" data-dismiss="modal"><b>&times;</b></button>
                      </div>
                      <div className="modal-body model-scroll">
                        <div className="row">
                            <div className="col-6">Object Id</div>
                            <div className="col-6">Brand Name</div>
                        </div>
                        <div className="row">
                          <div className="col-6">
                            <input
                              type="text"
                              id="popObjectId"
                              name="objectId"
                              className="form-control"
                              value={this.state.popUpData.objectId}
                              disabled
                            />
                            </div>
                            <div className="col-6">
                            <input
                              type="text"
                              name="brandName"
                              id="popBrandName"
                              className="form-control"
                              value={this.state.popUpData.brandName}
                              ref={el => (this.brandName = el)}
                              onChange={e => this.onChangeHandle(e)}
                            />
                            </div>
                        </div>
                        <div className="row">
                          <div className="col-6">Phone Number</div>
                          <div className="col-6">Tax %</div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                            <input
                              type="number"
                              id="popPhoneNumber"
                              className="form-control"
                              name="phoneNumber"
                              value={this.state.popUpData.phoneNumber}
                              ref={el => (this.phoneNumber = el)}
                              onChange={e => this.onChangeHandle(e)}
                            />
                          </div>
                          <div className="col-6">
                            <input
                              type="number"
                              id="popTaxPercentage"
                              name="tax"
                              className="form-control"  
                              value={this.state.popUpData.tax}
                              ref={el => (this.taxPercentage = el)}
                              onChange={e => this.onChangeHandle(e)}
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-6">Tax ID</div>
                          <div className="col-6">Pik %</div>
                        </div>
                        <div className="row">
                          <div className="col-6">
                          <input
                            type="text"
                            id="popTaxId"
                            name="taxId"
                            className="form-control"
                            value={this.state.popUpData.taxId}
                            ref={el => (this.taxId = el)}
                            onChange={e => this.onChangeHandle(e)}
                          />
                          </div>
                          <div className="col-6">
                          <input
                            type="number"
                            id="popPikPercentage"
                            className="form-control"
                            name="pik"
                            value={this.state.popUpData.pik}
                            ref={el => (this.pikPercentage = el)}
                            onChange={e => this.onChangeHandle(e)}
                          />
                          </div>
                        </div>
                        <Loader loading={loading} />
                        <div className="row">
                          <div className="col-6">Account Status</div>
                          <div className="col-6">Waived Amount</div>
                        </div>
                        <div className="row">
                          <div className="col-6">
                            <select
                              className="form-control"
                              id="popAccountStatus"
                              placeholder="Select Location"
                            >
                              <option
                              value={this.state.popUpData.accountStatus}
                              hidden
                              selected
                              >
                              {this.state.popUpData.accountStatus}
                              </option>
                              <option value="Active">Active</option>
                              <option value="Pending">Pending</option>
                              <option value="Deactivate">Deactivate</option>
                          </select>
                          </div>
                          <div className="col-6">
                          <input
                            type="number"
                            id="popWaivedAmount"
                            className="form-control"
                            name="waivedAmount"
                            value={this.state.popUpData.waivedAmount}
                            ref={el => (this.waivedAmount = el)}
                            onChange={e => this.onChangeHandle(e)}
                          />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-6">Balance Due</div>
                          <div className="col-6">Bank Charges</div>
                        </div>
                        <div className="row">
                          <div className="col-6">
                            <input
                              type="number"
                              id="popBalanceDue"
                              className="form-control"
                              name="balanceDue"
                              value={this.state.popUpData.balanceDue}
                              ref={el => (this.balanceDue = el)}
                              onChange={e => this.onChangeHandle(e)}
                            />
                          </div>
                          <div className="col-6">
                          <input
                            type="number"
                            id="popBankCharges"
                            className="form-control"
                            name="bankCharges"
                            value={this.state.popUpData.bankCharges}
                            ref={el => (this.bankCharges = el)}
                            onChange={e => this.onChangeHandle(e)}
                          />
                          </div>
                        </div>
                        </div>
                        <div className="modal-footer">
                          <div className="container">
                            <div className="row">
                              <div className="col-9 text-right">
                                <button type="button" id="closeEditOrder" data-dismiss="modal">Cancel</button>
                              </div>
                              <div className="col-3">
                                <button type="button" id="saveEditOrder" onClick={e => this.saveRow()} data-dismiss="modal">Save Changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            {/* Pop Up End Here */}
            <div className="row">
              <div className="col-lg-4">
                <PredefinedRanges id="dateRange" />
              </div>
              <div className="col-lg-4">
                <select
                  className="filter-note"
                  id="notes"
                  onChange={e => this.filterNotes(e)}
                >
                  <option value="">Notes</option>
                  <option value="objectId" id="object ID">
                    object ID
                  </option>
                  <option value="Business_name" id="Brand Name">
                    Brand Name
                  </option>
                  <option value="phoneNumber" id="Phone Number">
                    Phone Number
                  </option>
                  <option value="email" id="Email">
                    Email
                  </option>
                </select>
                <input
                  type="text"
                  id="filterByNotes"
                  className="filter-note-text"
                  placeholder="&nbsp; Filter by notes"
                  ref={el => (this.inputValue = el)}
                  disabled
                />
                <input
                  type="text"
                  id="input2"
                  size="1"
                  className="filter-note-iconBox notes-text-icon pointer"
                  onClick={() => this.reset()}
                />
              </div>
              <div className="col-lg-4">
                <select
                    className="filterByAccount"
                    id="account"
                    onChange={e => this.filterNotes(e)}
                  >
                  <option value="">Account Status</option>
                  <option value="activated">Active</option>
                  <option value="deActivated">DeActive</option>
                  <option value="pending">Pending</option>
                </select>
              </div>
            </div><br />
            <div className="row">
              <div className="col-lg-4">
                <select
                  className="filterByStudent"
                  id="student"
                >
                  <option value="">Student Support</option>
                  <option value="activated">Activated</option>
                  <option value="deActivated">DeActivated</option>
                </select>
              </div>
              <div className="col-2">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.search()}
                >
                  Search
                </button>
              </div>
              <div className="col-2">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.resetAll()}
                >
                  Reset
                </button>
              </div>
              <div className="col-4">
                <button
                  className="dropdown-toggle button-export"
                  data-toggle="dropdown"
                >
                  Export<span className="caret"></span>
                </button>
                <ul className="dropdown-menu dropdown-menu-right">
                  <li>
                    <a
                      href="#1"
                      id="notes"
                      className="pointer"
                      onClick={() => Excel.exportCsv(this.state.data)}
                    >
                      Microsoft Excel (.xlsx)
                    </a>
                  </li>
                  <li>
                    <a
                      href="#1"
                      id="notes"
                      className="pointer"
                      onClick={() => Pdf.exportPDF(this.state.data)}
                    >
                      PDF Document (.pdf)
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="row">
            <div className="col-lg-2 col-md-2 card-4">
                <p>Total Brand</p>
                <h2 className="text-center">
                  <CountUp end={Number(this.state.totalBrands)} />
                </h2>
              </div>
              <div className="col-lg-3 col-md-3 card-1">
                <p>Number of Shop</p>
                <h2 className="text-center">
                  <CountUp end={Number(this.state.numberOfShop)} />
                </h2>
              </div>
              <div className="col-lg-3 col-md-3 card-2">
                <p>Total Orders</p>
                <h2 className="text-center">
                  <CountUp end={Number(this.state.totalOrders)} />
                </h2>
              </div>
              <div className="col-lg-3 col-md-3 card-3">
                <p>Pending Verify</p>
                <h2 className="text-center">
                  <CountUp end={Number(this.state.pendingVerify)} />
                </h2>
              </div>
            </div>
            <Loader loading={loading} />
            <ReactTable
              data={this.state.data}
              columns={columns}
              defaultPageSize={5}
              pageSizeOptions={[3, 5, 15, 50, 100]}
            />
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default BusinessUser;
