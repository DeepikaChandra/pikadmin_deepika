import swal from 'sweetalert'

export const exportCsv = (data) => {
    const csvRow = [];
    const columnName = [
        ['object ID', 'Name', 'Email', 'Phone Number', 'Joined', 'Card', 'Country', 'Birthday']
    ];
    if (data.length === 0) {
        swal("Warning", "No Data Found", "warning")
        return;
    }
    for (let item = 0; item < data.length; item++) {
        columnName.push([data[item].objectId, data[item].name, data[item].email, data[item].phoneNumber,
            data[item].createdAt, data[item].card, data[item].country, data[item].birthday
        ]);
    }
    for (let i = 0; i < columnName.length; ++i) {
        csvRow.push(columnName[i].join(","))
    }
    const csvString = csvRow.join("%0A");
    const clickDownload = document.createElement("a");
    clickDownload.href = 'data:attachment/csv,' + csvString;
    clickDownload.terget = "_blank";
    clickDownload.download = "joinpik.csv";
    document.body.appendChild(clickDownload);
    swal("Success", "your file downloaded Successfully", "success")
    clickDownload.click();
}