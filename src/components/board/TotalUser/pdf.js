import swal from 'sweetalert'

export const exportPDF = (data) => {
    if (data.length === 0) {
        swal("Warning", "No data found...! Please import the data by changing the date", "warning")
        return;
    }
    var mywindow = window.open('', 'Print', 'height=600,width=800');
    mywindow.document.write('<html><head><title>Print</title><link media="all" />');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<h2 align="center"><i>Pik Admin Panel</i></h2>');
    mywindow.document.write('<table border="1" width="100%"><head>');
    mywindow.document.write('<tr><th>object ID</th><th>Name</th><th>Phone Number</th><th>Joined</th>' +
        '<th>Card</th><th>Country</th><th>Birthday</th></tr></head><tbody>')
    for (let i = 0; i < data.length; i++) {
        const objectId = data[i].objectId;
        const name = data[i].name;
        const PhoneNumber = data[i].phoneNumber;
        const createdAt = data[i].joined;
        const card = data[i].card;
        const country = data[i].country;
        const birthday = data[i].birthday;
        mywindow.document.write(
            '<tr><td>' + objectId + '</td><td>' + name + '</td><td>' + PhoneNumber + '</td>' +
            '<td>' + createdAt + '</td><td>' + card + '</td><td>' + country + '</td><td>' + birthday + '</td></tr>'
        );
    }
    mywindow.document.write('</tbody></table></body></html>');
    mywindow.document.close();
    mywindow.focus()
    mywindow.print();
    window.close();
    return true;
}