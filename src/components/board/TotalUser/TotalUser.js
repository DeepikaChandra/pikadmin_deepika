import React, { Component } from "react";
import MainLayout from "../../layouts/mainLayout";
import "react-table/react-table.css";
import log from "loglevel";
import Parse from "parse";
import "./TotalUser.css";
import _ from "lodash";
import CountUp from "react-countup";
import ReactTable from "react-table";
import PredefinedRanges from "../commonFile/js/dateRange/datePicker";
import * as Excel from "./excel";
import * as Pdf from "./pdf";
import * as date from "../commonFile/js/dateFormat";
import { Loader } from 'react-overlay-loader';
import 'react-overlay-loader/styles.css';

class TotalUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: "",
      savedCards: "",
      totalTransaction: "",
      currency: "",
      data: [],
      loading: true
    };
    this.filterNotes = this.filterNotes.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.onload();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onload() {
    this.setState({
      loading: true
    })
    var countryCode = document.getElementById("country").value;
    const dateRange = document.getElementById("dateRange").value;
    var fromDate = date.fromDate(dateRange);
    var toDate = date.toDate(dateRange);
    const country = Parse.Object.extend("Country");
    let countryQuery = new Parse.Query(country);
    countryQuery.equalTo("shortName", countryCode);
    let countryObj = [];
    countryQuery.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          countryObj.push(result[i].id);
        }
        let pointers = _.map(countryObj, function(objectId) {
          const pointer = new Parse.Object("Country");
          pointer.id = objectId;
          return pointer;
        });
        this.totalTransaction(pointers, fromDate, toDate);
        this.totalUser(pointers, fromDate, toDate);
        this.savedCards(pointers, fromDate, toDate);
        this.DataTable(pointers, fromDate, toDate);
      },
      error => {
        log.error(error.message);
      }
    );
  }

  DataTable(pointers, fromDate, toDate) {
    let notesValue = this.inputValue.value;
    const cards = document.getElementById("cards").value;
    const notes = document.getElementById("notes").value;
    const users = Parse.Object.extend("User");
    let usersObj = new Parse.Query(users);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      usersObj.containedIn("country", [pointers, undefined]);
    } else {
      usersObj.containedIn("country", pointers);
    }
    usersObj.equalTo("accountStatus", 2);
    usersObj.greaterThanOrEqualTo("createdAt", date.dateValidate(fromDate));
    usersObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    usersObj.limit(1000000);
    usersObj.include("country");
    //filter based on notes
    if (notes !== "" && notes !== undefined && notes === "objectId") {
      usersObj.matches("objectId", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "name") {
      usersObj.matches("name", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "phoneNumber") {
      usersObj.equalTo("phoneNumber", Number(notesValue));
    } else if (notes !== "" && notes !== undefined && notes === "email") {
      usersObj.equalTo("emailStr", notesValue);
    }
    //filter based on cards
    if (cards !== "" && cards !== undefined && cards === "Active") {
      usersObj.equalTo("cardAdded", 1);
    } else if (cards !== "" && cards !== undefined && cards === "inActive") {
      usersObj.equalTo("cardAdded", 0);
    }
    usersObj.find().then(
      result => {
        let dataList = [];
        for (let i = 0; i < result.length; i++) {
          const object = result[i];
          let objectId;
          let name;
          let phoneNumber;
          let email;
          let joined;
          let cardAdded;
          let country;
          let birthday;

          if (object.id !== undefined && object.id !== "") {
            objectId = object.id;
          } else {
            objectId = "";
          }
          if (object.get("name") !== undefined && object.get("name") !== "") {
            name = object.get("name");
          } else {
            name = "";
          }
          if (
            object.get("phoneNumber") !== undefined &&
            object.get("phoneNumber") !== ""
          ) {
            phoneNumber = object.get("phoneNumber");
          } else {
            phoneNumber = "";
          }
          if (
            object.get("emailStr") !== undefined &&
            object.get("emailStr") !== ""
          ) {
            email = object.get("emailStr");
          } else {
            email = "";
          }
          if (
            object.get("createdAt") !== undefined &&
            object.get("createdAt") !== ""
          ) {
            joined = date.dateFormat(object.get("createdAt"));
          } else {
            joined = "";
          }
          if (
            object.get("cardAdded") !== undefined &&
            object.get("cardAdded") !== ""
          ) {
            cardAdded = object.get("cardAdded");
            if (cardAdded === 1) {
              cardAdded = "Active";
            } else {
              cardAdded = "Inactive";
            }
          } else {
            cardAdded = "";
          }
          if (
            object.get("birthday") !== undefined &&
            object.get("birthday") !== ""
          ) {
            birthday = date.dateFormat(object.get("birthday"));
          } else {
            birthday = "";
          }
          if (
            object.get("country") !== undefined &&
            object.get("country") !== ""
          ){
            const countryPointer = object.get("country");
            if (
              countryPointer.get("country") !== undefined &&
              countryPointer.get("country") !== ""
            ){
            country = countryPointer.get("country");
            }else{
              country="";
            }
          } 
         
          const dataValue = {
            Sno: i + 1,
            objectId: objectId,
            name: name,
            phoneNumber: phoneNumber,
            email: email,
            joined: joined,
            card: cardAdded,
            country: country,
            birthday: birthday
          };
          dataList.push(dataValue);
        }
        this.setState({
          data: dataList,
          loading: false
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  totalUser(pointers, fromDate, toDate) {
    let count = 0;
    const users = Parse.Object.extend("User");
    let usersObj = new Parse.Query(users);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      usersObj.containedIn("country", [pointers, undefined]);
    } else {
      usersObj.containedIn("country", pointers);
    }
    usersObj.greaterThanOrEqualTo("createdAt", date.dateValidate(fromDate));
    usersObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    usersObj.equalTo("accountStatus", 2);
    usersObj.limit(1000000);
    usersObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          count = i + 1;
        }
        this.setState({
          users: count
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  savedCards(pointers, fromDate, toDate) {
    let count = 0;
    const cards = Parse.Object.extend("Cards");
    let cardsObj = new Parse.Query(cards);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      cardsObj.containedIn("country", [pointers, undefined]);
    } else {
      cardsObj.containedIn("country", pointers);
    }
    cardsObj.greaterThanOrEqualTo("createdAt", date.dateValidate(fromDate));
    cardsObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    cardsObj.limit(1000000);
    cardsObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          count = result.length;
        }
        this.setState({
          savedCards: count
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  totalTransaction(pointers, fromDate, toDate) {
    let totalTransaction = 0;
    let currency;
    const orderHistory = Parse.Object.extend("OrderHistory");
    let orderHistoryObj = new Parse.Query(orderHistory);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      orderHistoryObj.containedIn("country", [pointers, undefined]);
    } else {
      orderHistoryObj.containedIn("country", pointers);
    }
    orderHistoryObj.greaterThanOrEqualTo(
      "createdAt",
      date.dateValidate(fromDate)
    );
    orderHistoryObj.lessThanOrEqualTo("createdAt", date.dateValidate(toDate));
    orderHistoryObj.exists("totalCost");
    orderHistoryObj.exists("currency");
    orderHistoryObj.equalTo("orderStatus", 3);
    orderHistoryObj.limit(1000000);
    orderHistoryObj.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          if (
            result[i].get("totalCost") !== undefined &&
            result[i].get("totalCost") !== ""
          ){
          totalTransaction += result[i].get("totalCost");
          }
          if (
            result[i].get("currency") !== undefined &&
            result[i].get("currency") !== ""
          ){
          currency = result[i].get("currency");
          }else{
            currency="";
          }
        }
        this.setState({
          totalTransaction: totalTransaction,
          currency: currency
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  filterNotes(e) {
    document.getElementById("filterByNotes").disabled = false;
  }

  search() {
    this.onload();
  }

  resetAll(){
    window.location.reload();
  }

  reset() {
    document.getElementById("notes").value = "";
    this.inputValue.value = "";
    document.getElementById("filterByNotes").disabled = true;
    this.onload();
  }

  render() {
    const { loading } = this.state;
    const columns = [
      {
        Header: "",
        accessor: "Sno"
      },
      {
        Header: "object ID",
        accessor: "objectId"
      },
      {
        Header: "Name",
        accessor: "name"
      },
      {
        Header: "Phone Number",
        accessor: "phoneNumber"
      },
      {
        Header: "Email",
        accessor: "email"
      },
      {
        Header: "Joined",
        accessor: "joined"
      },
      {
        Header: "Card",
        accessor: "card"
      },
      {
        Header: "Country",
        accessor: "country"
      },
      {
        Header: "Birthday",
        accessor: "birthday"
      }
    ];
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            <div className="row">
              <div className="col-4">
                <PredefinedRanges id="dateRange" />
              </div>
              <div className="col-4">
                <select
                  className="filter-note"
                  id="notes"
                  onChange={e => this.filterNotes(e)}
                >
                  <option value="">Notes</option>
                  <option value="objectId" id="object ID">
                    object ID
                  </option>
                  <option value="name" id="Name">
                    Name
                  </option>
                  <option value="phoneNumber" id="Phone Number">
                    Phone Number
                  </option>
                  <option value="email" id="Email">
                    Email
                  </option>
                </select>
                <input
                  type="text"
                  id="filterByNotes"
                  className="filter-note-text"
                  placeholder="&nbsp; Filter by notes"
                  ref={el => (this.inputValue = el)}
                  disabled
                />
                <input
                  type="text"
                  id="input2"
                  size="1"
                  className="filter-note-iconBox notes-text-icon pointer"
                  onClick={() => this.reset()}
                />
              </div>
              <div className="col-4">
              <select
                  className="filterByCards"
                  id="cards"
                  onChange={e => this.filterNotes(e)}
                >
                <option value="">Cards</option>
                <option value="Active">Active</option>
                <option value="inActive">InActive</option>
                </select>
              </div>
            </div><br />
            <div className="row">
              <div className="col-4">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.search()}
                >
                  Search
                </button>
              </div>
              <div className="col-4">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.resetAll()}
                >
                  Reset
                </button>
              </div>
              <div className="col-4">
                <button
                  className="dropdown-toggle button-export"
                  data-toggle="dropdown"
                >
                  Export<span className="caret"></span>
                </button>
                <ul className="dropdown-menu dropdown-menu-right">
                  <li>
                    <a
                      href="pdf"
                      id="notes"
                      className="pointer"
                      onClick={() => Excel.exportCsv(this.state.data)}
                    >
                      Microsoft Excel (.xlsx)
                    </a>
                  </li>
                  <li>
                    <a
                      href="excel"
                      id="notes"
                      className="pointer"
                      onClick={() => Pdf.exportPDF(this.state.data)}
                    >
                      PDF Document (.pdf)
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 card-1">
                <p>Total Transaction</p>
                <h4 className="text-center">
                  <CountUp
                    decimals={2}
                    end={Number(this.state.totalTransaction)}
                  />
                  {this.state.currency}
                </h4>
              </div>
              <div className="col-lg-3 col-md-3 card-2">
                <p>Total Users</p>
                <h4 className="text-center">
                  <CountUp end={Number(this.state.users)} />
                </h4>
              </div>
              <div className="col-lg-3 col-md-3 card-3">
                <p>Saved Cards</p>
                <h4 className="text-center">
                  <CountUp end={Number(this.state.savedCards)} />
                </h4>
              </div>
            </div>
            <Loader loading={loading} />
            <ReactTable
              data={this.state.data}
              columns={columns}
              defaultPageSize={5}
              pageSizeOptions={[3, 5, 15, 50, 100]}
            />
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default TotalUser;