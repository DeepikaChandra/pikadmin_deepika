import React, { Component } from "react";
import MainLayout from "../../../layouts/mainLayout";
import "./CompletedOrders.css";
import log from "loglevel";
import Parse from "parse";
import _ from "lodash";
import CountUp from "react-countup";
import ReactTable from "react-table";
import PredefinedRanges from "../../commonFile/js/dateRange/datePicker";
import * as Excel from "./excel";
import * as Pdf from "./pdf";
import * as date from "../../commonFile/js/dateFormat";
import { Loader } from 'react-overlay-loader';
import { Multiselect } from "multiselect-react-dropdown";

class CompletedOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transaction: "",
      totalCollected: "",
      netSales: "",
      shopLocations: [],
      locationList: "",
      selectedLocation: [],
      loading: true
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.onload();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onload() {
    this.setState({
      loading: true
    })
    let countryShortName = document.getElementById("country").value;
    const dateRange = document.getElementById("dateRange").value;
    let fromDate = date.fromDate(dateRange);
    let toDate = date.toDate(dateRange);
    // Get the short name from country class based on select country
    const country = Parse.Object.extend("Country");
    let countryQuery = new Parse.Query(country);
    countryQuery.equalTo("shortName", countryShortName);
    let countryObj = [];
    countryQuery.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          countryObj.push(result[i].id);
        }
        // Fetch the all object id by using short Name from country class
        let pointers = _.map(countryObj, function(objectId) {
          const pointer = new Parse.Object("Country");
          pointer.id = objectId;
          return pointer;
        });
        this.TransactionAmountAndSales(pointers, fromDate, toDate);
        this.DataTable(pointers, fromDate, toDate);
      },
      error => {
        log.error(error.message);
      }
    );
  }

  TransactionAmountAndSales(pointers, fromDate, toDate) {
    var orderTypes = document.getElementById("orderTypes").value;
    let platform = document.getElementById("platform").value;
    let notesValue = this.inputValue.value;
    const notes = document.getElementById("notes").value;
    const orderHistory = Parse.Object.extend("OrderHistory");
    let orderHistoryObj = new Parse.Query(orderHistory);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      orderHistoryObj.containedIn("country", [pointers, undefined]);
    } else {
      orderHistoryObj.containedIn("country", pointers);
    }
    orderHistoryObj.greaterThanOrEqualTo(
      "orderTime",
      date.dateValidate(fromDate)
    );
    orderHistoryObj.lessThanOrEqualTo(
      "orderTime", 
      date.dateValidate(toDate)
      );
    // Business wise filter the data
    let businessName = document.getElementById("searchForBusiness").value;
    if (businessName !== undefined && businessName !== "") {
        const users = Parse.Object.extend("User");
        let innerUsersQuery = new Parse.Query(users);
        innerUsersQuery.matches("Business_name", businessName);
        orderHistoryObj.matchesQuery("owner", innerUsersQuery);
        // locationWise filter the Data
        let locationList = this.state.selectedLocation;
        if (locationList !== null && locationList.length > 0) {
            let ShopObject = Parse.Object.extend("ShopLocations");
            let innerShopQuery = new Parse.Query(ShopObject);
            innerShopQuery.containedIn("locationName", locationList);
            orderHistoryObj.matchesQuery("shop", innerShopQuery);
        }
    }
    // Query for fetching the data from pointer
    orderHistoryObj.include("user");
    orderHistoryObj.include("shop");
    orderHistoryObj.include("owner");
    orderHistoryObj.include("offerDetails");
    // Query for Platform
    if (
      platform !== "" &&
      platform === "PF"
    ) {
      orderHistoryObj.equalTo("userPlatform", platform);
    } else if (
        platform !== "" &&
        platform === "CP"
    ) {
      orderHistoryObj.containedIn("userPlatform", [platform, undefined]);
    } else if (
        platform !== "" &&
        platform === "BN"
    ) {
      orderHistoryObj.equalTo("userPlatform", platform);
    }
    // Query for filter based on payment
    if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "allOrderTypes"
    ) {
      orderHistoryObj.exists("orderStatus");
    } else if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "successfulOrders"
    ) {
      orderHistoryObj.equalTo("orderStatus", 3);
    } else if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "cancelOrders"
    ) {
      orderHistoryObj.equalTo("orderStatus", 4);
    } else if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "notVerified"
    ) {
      orderHistoryObj.equalTo("orderStatus", 5);
    }
    //Query for filter the data based on Notes
    if (notes !== "" && notes !== undefined && notes === "card") {
      orderHistoryObj.matches("cardLast4", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "gateWays") {
      orderHistoryObj.matches("gateway", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "orderId") {
      orderHistoryObj.matches("orderNo", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "name") {
      const UserObject = Parse.Object.extend("User");
      let userInnerQuery = new Parse.Query(UserObject);
      userInnerQuery.matches("name", notesValue);
      orderHistoryObj.matchesQuery("user", userInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "promoCode") {
      const offerObject = Parse.Object.extend("Offers");
      let offerInnerQuery = new Parse.Query(offerObject);
      offerInnerQuery.matches("offerCode", notesValue);
      orderHistoryObj.matchesQuery("offerDetails", offerInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "phoneNumber") {
      const shopObject = Parse.Object.extend("ShopLocations");
      let shopInnerpQuery = new Parse.Query(shopObject);
      shopInnerpQuery.equalTo("phoneNo", Number(notesValue));
      orderHistoryObj.matchesQuery("shop", shopInnerpQuery);
    }else if (notes !== "" && notes !== undefined && notes === "total") {
      orderHistoryObj.equalTo("totalCost", Number(notesValue));
    }

    
    orderHistoryObj.limit(1000000);
    orderHistoryObj.find().then(
      result => {
        let transaction = 0;
        let totalCollected = 0;
        let netSales = 0;
        for (let i = 0; i < result.length; i++) {
          transaction = i + 1;
          if (result[i].get("subTotal") !== undefined) {
            totalCollected += parseFloat(result[i].get("subTotal"));
          }
          if (result[i].get("totalCost") !== undefined) {
            netSales += parseFloat(result[i].get("totalCost"));
          }
        }

        this.setState({
          transaction: transaction,
          totalCollected: totalCollected,
          netSales: netSales
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  DataTable(pointers, fromDate, toDate) {
    var orderTypes = document.getElementById("orderTypes").value;
    let platform = document.getElementById("platform").value;
    let notesValue = this.inputValue.value;
    const notes = document.getElementById("notes").value;
    const orderHistory = Parse.Object.extend("OrderHistory");
    let orderHistoryObj = new Parse.Query(orderHistory);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      orderHistoryObj.containedIn("country", [pointers, undefined]);
    } else {
      orderHistoryObj.containedIn("country", pointers);
    }
    orderHistoryObj.greaterThanOrEqualTo(
      "orderTime",
      date.dateValidate(fromDate)
    );
    orderHistoryObj.lessThanOrEqualTo("orderTime", date.dateValidate(toDate));
    // Business wise filter the data
    let businessName = document.getElementById("searchForBusiness").value;
    if (businessName !== undefined && businessName !== "") {
        const users = Parse.Object.extend("User");
        let innerUsersQuery = new Parse.Query(users);
        innerUsersQuery.matches("Business_name", businessName);
        orderHistoryObj.matchesQuery("owner", innerUsersQuery);
        // locationWise filter the Data
        let locationList = this.state.selectedLocation;
        if (locationList !== null && locationList.length > 0) {
            let ShopObject = Parse.Object.extend("ShopLocations");
            let innerShopQuery = new Parse.Query(ShopObject);
            innerShopQuery.containedIn("locationName", locationList);
            orderHistoryObj.matchesQuery("shop", innerShopQuery);
        }
    }
    // Query for fetching the data from pointer
    orderHistoryObj.include("user");
    orderHistoryObj.include("shop");
    orderHistoryObj.include("owner");
    orderHistoryObj.include("offerDetails");
    // Query for Platform
    if (
      platform !== "" &&
      platform === "PF"
    ) {
      orderHistoryObj.equalTo("userPlatform", platform);
    } else if (
      platform !== "" &&
      platform === "CP"
    ) {
      orderHistoryObj.containedIn("userPlatform", [platform, undefined]);
    } else if (
      platform !== "" &&
      platform === "BN"
    ) {
      orderHistoryObj.equalTo("userPlatform", platform);
    }
    // Query for filter based on payment
    if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "allOrderTypes"
    ) {
      orderHistoryObj.exists("orderStatus");
    } else if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "successfulOrders"
    ) {
      orderHistoryObj.equalTo("orderStatus", 3);
    } else if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "cancelOrders"
    ) {
      orderHistoryObj.equalTo("orderStatus", 4);
    } else if (
      orderTypes !== "" &&
      orderTypes !== undefined &&
      orderTypes === "notVerified"
    ) {
      orderHistoryObj.equalTo("orderStatus", 5);
    }
    //Query for filter the data based on Notes
    if (notes !== "" && notes !== undefined && notes === "card") {
      orderHistoryObj.matches("cardLast4", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "gateWays") {
      orderHistoryObj.matches("gateway", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "orderId") {
      orderHistoryObj.matches("orderNo", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "name") {
      const UserObject = Parse.Object.extend("User");
      let userInnerQuery = new Parse.Query(UserObject);
      userInnerQuery.matches("name", notesValue);
      orderHistoryObj.matchesQuery("user", userInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "promoCode") {
      const offerObject = Parse.Object.extend("Offers");
      let offerInnerQuery = new Parse.Query(offerObject);
      offerInnerQuery.matches("offerCode", notesValue);
      orderHistoryObj.matchesQuery("offerDetails", offerInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "phoneNumber") {
      const shopObject = Parse.Object.extend("ShopLocations");
      let shopInnerpQuery = new Parse.Query(shopObject);
      shopInnerpQuery.equalTo("phoneNo", Number(notesValue));
      orderHistoryObj.matchesQuery("shop", shopInnerpQuery);
    }else if (notes !== "" && notes !== undefined && notes === "total") {
      orderHistoryObj.equalTo("totalCost", Number(notesValue));
    }
    orderHistoryObj.limit(1000000);
    orderHistoryObj.exists("owner");
    orderHistoryObj.exists("shop");
    orderHistoryObj.find().then(
      result => {
        let dataList = [];
        let sno=0;
        
        for (let i = 0; i < result.length; i++) {
          let shopPointer = "";
          let orderTime='';
          let email='';
          let userPointer='';
          let offerPoiter='';
          let locationName='';
          let businessPointer = "";
          let card='';
          let customerName='';
          let discountGiven='';
          let vat=0;
          let orderType='';
          let cancelledBy='';
          let refundCharges=0;
          let cancelNotes='';
          let orderNotes='';
          let total=0;
          let subTotal=0;
          let time='';
          let percentage=0;
          let pikCharges=0;
          let promoCode='';
          let orderNo='';
          let gateway='';
          let discountAmount=0;
          let customerNumber='';
          let shopNumber;
          let orderStatus='';
          let businessNumber='';
          let pikPercentageUser='';
            let commissionFee='';
          if (result[i].get('orderTime') !== undefined && result[i].get('orderTime') !== "") { // current date and order date
            orderTime = date.dateFormat(result[i].get('orderTime'));
         } else {
           orderTime = "";
         }
     
            if (result[i].get("owner") !== undefined && result[i].get("owner") !== "") {
              if (
                result[i].get("subTotal") !== undefined &&
                result[i].get("subTotal") !== ""
              ) {
                subTotal = Number(result[i].get("subTotal"));
              }
              businessPointer = result[i].get("owner"); // Business name
                
                if (
                    businessPointer.get("Business_name") !== undefined &&
                    businessPointer.get("Business_name") !== ""
                ) {
                    businessName = businessPointer.get("Business_name");
                } else {
                    businessName = "";
                }
                if (
                  businessPointer.get("phoneNumber") !== undefined &&
                  businessPointer.get("phoneNumber") !== ""
              ) {
                 businessNumber = businessPointer.get("phoneNumber");
               } else {
                 businessNumber = "";
              }
              if (
                businessPointer.get("pikPercentage") !== undefined &&
                businessPointer.get("pikPercentage") !== ""
            ) {
              pikPercentageUser = businessPointer.get("pikPercentage");
              commissionFee= (subTotal * pikPercentageUser)/100;
             } else {
              pikPercentageUser = "";
            }
            }
            if (result[i].get("shop") !== undefined && result[i].get("shop") !== "") {
                shopPointer = result[i].get("shop"); // Location name
                if (shopPointer.get("locationName") !== undefined &&
                  shopPointer.get("locationName") !== '') 
                  {
                    locationName = shopPointer.get("locationName");
                } else {
                    locationName = "No Shop";
                }
                if (
                  shopPointer.get("phoneNo") !== undefined &&
                  shopPointer.get("phoneNo") !== ""
                ) {
                  shopNumber = shopPointer.get("phoneNo");
                } else {
                  shopNumber = "";
                }
            } else {
                locationName = "No Shop";
            }
            if (result[i].get("email") !== undefined && result[i].get("email") !== "") {
              email = result[i].get("email");
            }
              if (
                result[i].get("cardLast4") !== undefined &&
                result[i].get("cardLast4") !== ""
              ) {
                card = result[i].get("cardLast4") ;
              } 
              if (result[i].get("user") !== undefined && 
                 result[i].get("user") !== "") {
                userPointer = result[i].get("user"); // user poiter class
                if (userPointer.get("name") !== undefined  &&
                userPointer.get("name") !== "" ) {
                  customerName = userPointer.get("name");
                } 
                if (userPointer.get("phoneNo") !== undefined  &&
                userPointer.get("phoneNo") !== "") {
                  customerNumber = userPointer.get("phoneNo");
                } 
              }
              if (
                result[i].get("discountPaidBy") !== undefined &&
                result[i].get("discountPaidBy") !== ""
              ) {
                discountGiven = result[i].get("discountPaidBy");
              } 
              if (
                result[i].get("discountAmount") !== undefined &&
                result[i].get("discountAmount") !== ""
              ) {
                discountAmount = result[i].get("discountAmount");
              } 
              if (result[i].get("notes") !== undefined && result[i].get("notes") !== "") {
                orderNotes = result[i].get("notes");
              } 
              if (
                result[i].get("cancelNote") !== undefined &&
                result[i].get("cancelNote") !== ""
              ) {
                cancelNotes = result[i].get("cancelNote");
              }
              if (
                result[i].get("refundCost") !== undefined &&
                result[i].get("refundCost") !== ""
              ) {
                refundCharges = Number(result[i].get("refundCost"));
              } 
              if (
                result[i].get("cancelledBy") !== undefined &&
                result[i].get("cancelledBy") !== ""
              ) {
                cancelledBy = result[i].get("cancelledBy");
              }
              if (
                result[i].get("orderStatus") !== undefined &&
                result[i].get("orderStatus") !== ""
              ) {
                orderStatus = Number(result[i].get("orderStatus"));
                if(orderStatus===3){
                  orderType="success "
                }else if(orderStatus===4){
                  orderType="cancelled "
                }else if(orderStatus===5){
                  orderType="Not verified "
                }
              }
              if (result[i].get("tax") !== undefined && result[i].get("tax") !== "") {
                vat = Number(result[i].get("tax"));
              } 
              if (
                result[i].get("totalCost") !== undefined &&
                result[i].get("totalCost") !== ""
              ) {
                total = Number(result[i].get("totalCost"));
              } 
              
              if (
                result[i].get("totalTime") !== undefined &&
                result[i].get("totalTime") !== ""
              ) {
                time = Number(result[i].get("totalTime"));
              } 
              if (
                result[i].get("pikCharges") !== undefined &&
                result[i].get("pikCharges") !== ""
              ) {
                pikCharges = Number(result[i].get("pikCharges"));
              } 
              if (
                result[i].get("pikCharges") !== undefined &&
                result[i].get("pikPercentage") !== ""
              ) {
                percentage = Number(result[i].get("pikPercentage"));
              } 
              if (
                result[i].get("offerDetails") !== undefined &&
                result[i].get("offerDetails") !== ""
              ) {
                offerPoiter = result[i].get("offerDetails"); // offerDetails pointer class
                if (
                  offerPoiter.get("offerCode") !== undefined ||
                  offerPoiter.get("offerCode") !== ""
                ) {
                  promoCode = offerPoiter.get("offerCode");
                }
              }
              if (
                result[i].get("gateway") !== undefined &&
                result[i].get("gateway") !== ""
              ) {
                gateway = result[i].get("gateway");
              }
              if (
                result[i].get("orderNo") !== undefined &&
                result[i].get("orderNo") !== ""
              ) {
                orderNo = result[i].get("orderNo");
              }

          sno = sno + 1;
          const dataValue = {
            Sno: sno,
            orderTime: orderTime,
            customerName: customerName,
            shopName: businessName,
            shopLocation: locationName,
            shopNumber: shopNumber,
            customerNumber:customerNumber,
            businessNumber:businessNumber,
            email: email,
            discountGiven: discountGiven,
            card: card,
            orderNotes: orderNotes,
            cancelNotes: cancelNotes,
            percentage: percentage,
            pikCharges: pikCharges,
            refundCharges: refundCharges,
            cancelledBy: cancelledBy,
            orderType: orderType,
            vat: vat,
            subTotal: subTotal,
            time: time,
            total: total,
            promoCode: promoCode,
            orderNo: orderNo,
            gateway: gateway,
            discountAmount:discountAmount,
            commissionFee:commissionFee
          };
          dataList.push(dataValue);
        }
        this.setState({
          data: dataList,
          loading: false
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }
  searchByBusiness(e) {
    const users = Parse.Object.extend("User");
    let usersQuery = new Parse.Query(users);
    if (e.target.value === "") {
        this.setState({
            selectedLocation: [],
            shopLocations: []
        });
        this.onload();
    }
    usersQuery.exists("Business_name");
    usersQuery.equalTo("Business_name", e.target.value);
    let businessObjId = [];
    usersQuery.find().then(
        result => {
            for (let i = 0; i < result.length; i++) {
                businessObjId.push(result[i].id);
            }
            // query for filter by date and country
            let countryCode = document.getElementById("country").value;
            const dateRange = document.getElementById("dateRange").value;
            let fromDate = date.fromDate(dateRange);
            let toDate = date.toDate(dateRange);
            const country = Parse.Object.extend("Country");
            let countryQuery = new Parse.Query(country);
            countryQuery.equalTo("shortName", countryCode);
            let countryObj = [];
            countryQuery.find().then(result => {
                for (let i = 0; i < result.length; i++) {
                    countryObj.push(result[i].id);
                }
                this.DataTable(countryObj, fromDate, toDate);
                this.filterByLocation(businessObjId);
            });
        },
        error => {
            log.error(error.message);
        }
    );
}

filterByLocation(businessObjId) {
    const shop = Parse.Object.extend("ShopLocations");
    let shopLocationsQuery = new Parse.Query(shop);
    shopLocationsQuery.containedIn("business", businessObjId);
    let shopLocations = [];
    shopLocationsQuery.find().then(
        result => {
            for (let i = 0; i < result.length; i++) {
                const dataValue = {
                    id: result[i].get("locationName"),
                    label: result[i].get("locationName")
                };
                shopLocations.push(dataValue);
            }
            this.setState({
                shopLocations: shopLocations
            });
        },
        error => {
            log.error(error.message);
        }
    );
}

  filterNotes(e) {
    document.getElementById("filterByNotes").disabled = false;
  }

  reset() {
    document.getElementById("notes").value = "";
    this.inputValue.value = "";
    document.getElementById("filterByNotes").disabled = true;
    this.onload();
  }

  search() {
    this.onload();
  }

  resetAll() {
    window.location.reload()
  }

  onSelect(e){
    let selectedLocation = [];
    for(let i in e){
        selectedLocation.push(e[i].id)
    } 
    this.setState({
        selectedLocation: selectedLocation
    })
}
onRemove(e){
  let selectedLocation = [];
  for(let i in e){
      selectedLocation.push(e[i].id)
  } 
  this.setState({
      selectedLocation: selectedLocation
  })
}

  render() {
    const columns = [
      {
        Header: "S.no",
        accessor: "Sno"
      },
      {
        Header: "Date",
        accessor: "orderTime"
      },
      {
        Header: "Name",
        accessor: "customerName"
      },
      {
        Header: "Shop Name",
        accessor: "shopName"
      },
      {
        Header: "Shop Location",
        accessor: "shopLocation"
      },
      {
        Header: "Order ID",
        accessor: "orderNo"
      },
      {
        Header: "Time",
        accessor: "time"
      },
      {
        Header: "Sub Total",
        accessor: "subTotal"
      },
      {
        Header: "Total",
        accessor: "total"
      },
      {
        Header: "Vat",
        accessor: "vat"
      },
      {
        Header: "Order Type",
        accessor: "orderType"
      },
      {
        Header: "Cancelled By",
        accessor: "cancelledBy"
      },
      {
        Header: "Refund Charges",
        accessor: "refundCharges"
      },
      {
        Header: "Cancel Notes",
        accessor: "cancelNotes"
      },
      {
        Header: "Order Notes",
        accessor: "orderNotes"
      },
      {
        Header: "Order Time",
        accessor: "orderTime"
      },
      {
        Header: "Discount Given",
        accessor: "discountGiven"
      },
      {
        Header: "Discount Amount",
        accessor: "discountAmount"
      },
      {
        Header: "Business Ph Number",
        accessor: "businessNumber"
      },
      {
        Header: "Shop Ph Number",
        accessor: "shopNumber"
      },
      {
        Header: "Customer Ph Number",
        accessor: "customerNumber"
      },
      {
        Header: "Email",
        accessor: "email"
      },
      {
        Header: "Card",
        accessor: "card"
      },
      {
        Header: "Promo Code",
        accessor: "promoCode"
      },
      {
        Header: "Payment Gateways",
        accessor: "gateway"
      },
      {
        Header: "Pik Charges",
        accessor: "pikCharges"
      },
      {
        Header: "Pik Percentage",
        accessor: "percentage"
      }
    ];
    const { loading } = this.state;
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            <div className="row">
              <div className="col-lg-4">
                <PredefinedRanges id="dateRange" />
              </div>
              <div className="col-lg-2">
                <select
                  className="custom-select"
                  id="orderTypes"
                >
                  <option value="">Order Types</option>
                  <option value="allOrderTypes" id="allOrdertTypes">
                    All Orders
                  </option>
                  <option value="successfulOrders" id="successfulOrders">
                    Successful orders
                  </option>
                  <option value="cancelOrders" id="cancelOrders">
                    Cancel order
                  </option>
                  <option value="notVerified" id="notVerified">
                    Not verified
                  </option>
                </select>
              </div>
              <div className="col-2">
                <select
                  className="custom-select"
                  id="platform"
                >
                  <option value="" hidden>Platform</option>
                  <option value="PF" id="PF">
                    Pik Food
                  </option>
                  <option value="CP" id="CP">
                    Coffee Pik
                  </option>
                  <option value="BN" id="BN">
                    Beans
                  </option>
                  <option value="" id="">
                    All (Platform)
                  </option>
                </select>
              </div>
              <div className="col-lg-4">
                <select
                  className="filter-note"
                  id="notes"
                  onChange={e => this.filterNotes(e)}
                >
                  <option value="">Notes</option>
                  <option value="name" id="name">
                    Name
                  </option>
                  <option value="phoneNumber" id="phoneNumber">
                    Phone Number
                  </option>
                  <option value="card" id="card">
                    Card
                  </option>
                  <option value="orderId" id="orderId">
                    Order ID
                  </option>
                  <option value="promoCode" id="promoCode">
                    Promo Code
                  </option>
                  <option value="gateWays" id="gateWays">
                    Gateways
                  </option>
                  <option value="total" id="total">
                    Total
                  </option>
                </select>
                <input
                  type="text"
                  id="filterByNotes"
                  className="filter-note-text"
                  placeholder="&nbsp; Filter by notes"
                  ref={el => (this.inputValue = el)}
                  disabled
                />
                <input
                  type="text"
                  id="input2"
                  size="1"
                  className="filter-note-iconBox notes-text-icon pointer"
                  onClick={() => this.reset()}
                />
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-lg-4">
                <input className="search__input" id="searchForBusiness" onKeyUp={(e) => this.searchByBusiness(e)} placeholder="search" />
              </div>
              <div className="col-lg-4">
                <Multiselect
                  options={this.state.shopLocations}
                  displayValue="id"
                  onSelect={(e)=>this.onSelect(e)}
                  onRemove={(e)=>this.onRemove(e)}
                  placeholder="Select Location"
                  showCheckbox={false}
                  avoidHighlightFirstOption={true}
                  closeOnSelect={false}
                  style={{ chips: { background: "#673ab7" }, multiselectContainer: {color: "#673ab7"}}}
                />
              </div>
              <div className="col-lg-2">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.search()}
                >
                  Search
                </button>
              </div>
              <div className="col-lg-2">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.resetAll()}
                >
                  Reset
                </button>
              </div>
            </div>
            <br></br>
            <div className="row">
              <div className="col-8"></div>
              <div className="col-4">
                <button
                  className="dropdown-toggle button-export"
                  data-toggle="dropdown"
                >
                  Export<span className="caret"></span>
                </button>
                <ul className="dropdown-menu dropdown-menu-right">
                  <li>
                    <a
                      href="#excel"
                      id="notes"
                      className="pointer"
                      onClick={() => Excel.exportCsv(this.state.data)}
                    >
                      Microsoft Excel (.xlsx)
                    </a>
                  </li>
                  <li>
                    <a
                      href="#pdf"
                      id="notes"
                      className="pointer"
                      onClick={() => Pdf.exportPDF(this.state.data)}
                    >
                      PDF Document Orders (.pdf)
                    </a>
                  </li>
                </ul>
              </div>
            </div><br></br>
            <div className="row">
              <div className="col-lg-4 col-md-4">
                <h4>
                  <CountUp end={Number(this.state.transaction)} />
                  {this.state.currency}
                </h4>
                <p>TOTAL ORDERS</p>
              </div>
              <div className="col-lg-4 col-md-4">
                <h4>
                  <CountUp
                    decimals={2}
                    end={Number(this.state.totalCollected)}
                  />
                </h4>
                <p> TOTAL COLLECTED</p>
              </div>
              <div className="col-lg-4 col-md-4">
                <h4>
                  <CountUp decimals={2} end={Number(this.state.netSales)} />
                </h4>
                <p>NET SALES</p>
              </div>
            </div>
            <br />
            <Loader loading={loading} />
            <ReactTable
              data={this.state.data}
              columns={columns}
              defaultPageSize={10}
              pageSizeOptions={[3, 5, 10, 15, 50, 100]}
            />
          </div>
        </main>
      </MainLayout>
    );
  }
}

export default CompletedOrders;
