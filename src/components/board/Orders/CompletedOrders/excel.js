import swal from 'sweetalert'

export const exportCsv = (data) => {
    const csvRow = [];
    const columnName = [
        ['S.No', 'Date', 'Name', 'Shop','Shop location', 'Number', 'Total cost','Sub total','Email', 'Card', 'Order ID', 'Promo Code', 'GetWays', 'Commission Fee', 'Discount Amount']
    ];
    if (data.length === 0) {
        swal("Warning", "No Data Found", "warning")
        return;
    }
    for (let item = 0; item < data.length; item++) {
        columnName.push([data[item].Sno, data[item].orderTime, data[item].customerName, data[item].shopName,
            data[item].shopLocation, data[item].customerNumber,data[item].total,data[item].subTotal, data[item].email, data[item].card, data[item].orderNo, data[item].promoCode,
             data[item].gateway, data[item].commissionFee, data[item].discountAmount
        ]);
    }
    for (let i = 0; i < columnName.length; ++i) {
        csvRow.push(columnName[i].join(","))
    }
    const csvString = csvRow.join("%0A");
    const clickDownload = document.createElement("a");
    clickDownload.href = 'data:attachment/csv,' + csvString;
    clickDownload.terget = "_blank";
    clickDownload.download = "joinpik.csv";
    document.body.appendChild(clickDownload);
    clickDownload.click();
}