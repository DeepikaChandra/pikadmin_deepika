import swal from "sweetalert";

// Generate PDF for DataTable
export const exportPDF = data => {
  if (data.length === 0) {
    swal(
      "Warning",
      "No data found...! Please import the data by changing the date",
      "warning"
    );
    return;
  }
  var mywindow = window.open("", "Print", "height=600,width=800");
  mywindow.document.write(
    '<html><head><title>Print</title><link media="all" />'
  );
  mywindow.document.write("</head><body>");
  mywindow.document.write('<h2 align="center"><i>Pik Admin Panel</i></h2>');
  mywindow.document.write('<table border width="100%"><head>');
  mywindow.document.write(
    "<tr><th></th><th>Date</th><th>Name</th><th>Shop</th><th>Shop Number</th><th>Email</th>" +
      "<th>Card</th><th>Order ID</th><th>Promo Code</th><th>Gateway</th><th>Percentage</th></tr></head><tbody>"
  );
  for (let i = 0; i < data.length; i++) {
    mywindow.document.write(
      "<tr><td>" +
        data[i].Sno +
        "</td><td>" +
        data[i].orderTime +
        "</td><td>" +
        data[i].customerName +
        "</td><td>" +
        data[i].shopName +
        "</td>" +
        "<td>" +
        data[i].shopNumber +
        "</td><td>" +
        data[i].email +
        "</td><td>" +
        data[i].card +
        "</td><td>" +
        data[i].orderNo +
        "</td><td>" +
        data[i].promoCode +
        "</td><td>" +
        data[i].gateway +
        "</td><td>" +
        data[i].percentage +
        "</td></tr>"
    );
  }
  mywindow.document.write("</tbody></table></body></html>");
  mywindow.document.close();
  mywindow.focus();
  mywindow.print();
  window.close();
  return true;
};
