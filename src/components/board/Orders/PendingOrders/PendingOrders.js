import React, { Component } from "react";
import MainLayout from "../../../layouts/mainLayout";
import log from "loglevel";
import Parse from "parse";
import "./PendingOrder.scss"
import _ from "lodash";
import CountUp from "react-countup";
import ReactTable from "react-table";
import PredefinedRanges from "../../commonFile/js/dateRange/datePicker";
import * as date from "../../commonFile/js/dateFormat";
import { Loader } from 'react-overlay-loader';
import { Multiselect } from "multiselect-react-dropdown";
import { toast } from "react-toastify";
import '../../commonFile/css/loader.css';
import $ from 'jquery'; 
import swal from "sweetalert";

class CompletedOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalOrders: "",
      shopLocations: [],
      locationList: "",
      selectedLocation: [],
      data: [],
      popUpData: [],
      loading: true
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.onload();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
    // toast pop up
    taostMessage(message) {
      toast.warning(message, {
        position: toast.POSITION.TOP_CENTER,
      });
    }
  onload() {
    this.setState({
      loading: true
    })
    let countryShortName = document.getElementById("country").value;
    const dateRange = document.getElementById("dateRange").value;
    let fromDate = date.fromDate(dateRange);
    let toDate = date.toDate(dateRange);
    // Get the short name from country class based on select country
    const country = Parse.Object.extend("Country");
    let countryQuery = new Parse.Query(country);
    countryQuery.equalTo("shortName", countryShortName);
    let countryObj = [];
    countryQuery.find().then(
      result => {
        for (let i = 0; i < result.length; i++) {
          countryObj.push(result[i].id);
        }
        // Fetch the all object id by using short Name from country class
        let pointers = _.map(countryObj, function(objectId) {
          const pointer = new Parse.Object("Country");
          pointer.id = objectId;
          return pointer;
        });
        this.totalOrders(pointers, fromDate, toDate);
        this.DataTable(pointers, fromDate, toDate);
      },
      error => {
        log.error(error.message);
      }
    );
  }

  totalOrders(pointers, fromDate, toDate) {
    let orderStatus = document.getElementById("orderStatus").value;
    let platform = document.getElementById("platform").value;
    let notesValue = this.inputValue.value;
    const notes = document.getElementById("notes").value;
    const Orders = Parse.Object.extend("Orders");
    let ordersObj = new Parse.Query(Orders);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      ordersObj.containedIn("country", [pointers, undefined]);
    } else {
      ordersObj.containedIn("country", pointers);
    }
    ordersObj.greaterThanOrEqualTo(
      "createdAt",
      date.dateValidate(fromDate)
    );
    ordersObj.lessThanOrEqualTo(
      "createdAt", 
      date.dateValidate(toDate)
      );
    // Business wise filter the data
    let businessName = document.getElementById("searchForBusiness").value;
    if (businessName !== undefined && businessName !== "") {
        const users = Parse.Object.extend("User");
        let innerUsersQuery = new Parse.Query(users);
        innerUsersQuery.matches("Business_name", businessName);
        ordersObj.matchesQuery("owner", innerUsersQuery);
        // locationWise filter the Data
        let locationList = this.state.selectedLocation;
        if (locationList !== null && locationList.length > 0) {
            let ShopObject = Parse.Object.extend("ShopLocations");
            let innerShopQuery = new Parse.Query(ShopObject);
            innerShopQuery.containedIn("locationName", locationList);
            ordersObj.matchesQuery("shop", innerShopQuery);
        }
    }
    // Query for fetching the data from pointer
    ordersObj.include("user");
    ordersObj.include("shop");
    ordersObj.include("owner");
    ordersObj.include("offerDetails");
    // Query for Platform
    if (
      platform !== "" &&
      platform === "PF"
    ) {
      ordersObj.equalTo("userPlatform", platform);
    } else if (
      platform !== "" &&
      platform === "CP"
    ) {
      ordersObj.containedIn("userPlatform", [platform, undefined]);
    } else if (
      platform !== "" &&
      platform === "BN"
    ) {
      ordersObj.equalTo("userPlatform", platform);
    }
    // Query for filter based on Order Status
    if (
      orderStatus !== "" &&
      orderStatus === "notStarted"
    ) {
      ordersObj.equalTo("orderStatus", 0);
    } else if (
      orderStatus !== "" &&
      orderStatus === "inProgress"
    ) {
      ordersObj.equalTo("orderStatus", 1);
    } else if (
      orderStatus !== "" &&
      orderStatus === "readyForPickup"
    ) {
      ordersObj.equalTo("orderStatus", 2);
    }
    //Query for filter the data based on Notes
    if (notes !== "" && notes !== undefined && notes === "card") {
      ordersObj.matches("cardLast4", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "gateWays") {
      ordersObj.matches("gateway", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "orderId") {
      ordersObj.matches("objectId", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "name") {
      const UserObject = Parse.Object.extend("User");
      let userInnerQuery = new Parse.Query(UserObject);
      userInnerQuery.matches("name", notesValue);
      ordersObj.matchesQuery("user", userInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "promoCode") {
      const offerObject = Parse.Object.extend("Offers");
      let offerInnerQuery = new Parse.Query(offerObject);
      offerInnerQuery.matches("offerCode", notesValue);
      ordersObj.matchesQuery("offerDetails", offerInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "phoneNumber") {
      const shopObject = Parse.Object.extend("ShopLocations");
      let shopInnerpQuery = new Parse.Query(shopObject);
      shopInnerpQuery.equalTo("phoneNo", Number(notesValue));
      ordersObj.matchesQuery("shop", shopInnerpQuery);
    }else if (notes !== "" && notes !== undefined && notes === "total") {
      ordersObj.equalTo("totalCost", Number(notesValue));
    }
    ordersObj.limit(1000000);
    
    ordersObj.find().then(result => {
        let totalOrders = 0;
        for (let i = 0; i < result.length; i++) {
          totalOrders = i + 1;
        }

        this.setState({
          totalOrders: totalOrders
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }

  DataTable(pointers, fromDate, toDate) {
    let orderStatus = document.getElementById("orderStatus").value;
    let platform = document.getElementById("platform").value;
    let notesValue = this.inputValue.value;
    const notes = document.getElementById("notes").value;
    const Orders = Parse.Object.extend("Orders");
    let ordersObj = new Parse.Query(Orders);
    let countryCode = document.getElementById("country").value;
    if (countryCode === "AE") {
      ordersObj.containedIn("country", [pointers, undefined]);
    } else {
      ordersObj.containedIn("country", pointers);
    }
    ordersObj.greaterThanOrEqualTo(
      "createdAt",
      date.dateValidate(fromDate)
    );
    ordersObj.lessThanOrEqualTo(
      "createdAt", 
      date.dateValidate(toDate)
      );
    // Business wise filter the data
    let businessName = document.getElementById("searchForBusiness").value;
    if (businessName !== undefined && businessName !== "") {
        const users = Parse.Object.extend("User");
        let innerUsersQuery = new Parse.Query(users);
        innerUsersQuery.matches("Business_name", businessName);
        ordersObj.matchesQuery("owner", innerUsersQuery);
        // locationWise filter the Data
        let locationList = this.state.selectedLocation;
        if (locationList !== null && locationList.length > 0) {
            let ShopObject = Parse.Object.extend("ShopLocations");
            let innerShopQuery = new Parse.Query(ShopObject);
            innerShopQuery.containedIn("locationName", locationList);
            ordersObj.matchesQuery("shop", innerShopQuery);
        }
    }
    // Query for fetching the data from pointer
    ordersObj.include("user");
    ordersObj.include("shop");
    ordersObj.include("owner");
    ordersObj.include("offerDetails");
    // Query for Platform
    if (
      platform !== "" &&
      platform === "PF"
    ) {
      ordersObj.equalTo("userPlatform", platform);
    } else if (
      platform !== "" &&
      platform === "CP"
    ) {
      ordersObj.containedIn("userPlatform", [platform, undefined]);
    } else if (
      platform !== "" &&
      platform === "BN"
    ) {
      ordersObj.equalTo("userPlatform", platform);
    }
    // Query for filter based on Order Status
    if (
      orderStatus !== "" &&
      orderStatus === "notStarted"
    ) {
      ordersObj.equalTo("orderStatus", 0);
    } else if (
      orderStatus !== "" &&
      orderStatus === "inProgress"
    ) {
      ordersObj.equalTo("orderStatus", 1);
    } else if (
      orderStatus !== "" &&
      orderStatus === "readyForPickup"
    ) {
      ordersObj.equalTo("orderStatus", 2);
    }
    //Query for filter the data based on Notes
    if (notes !== "" && notes !== undefined && notes === "card") {
      ordersObj.matches("cardLast4", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "gateWays") {
      ordersObj.matches("gateway", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "orderId") {
      ordersObj.matches("objectId", notesValue);
    } else if (notes !== "" && notes !== undefined && notes === "name") {
      const UserObject = Parse.Object.extend("User");
      let userInnerQuery = new Parse.Query(UserObject);
      userInnerQuery.matches("name", notesValue);
      ordersObj.matchesQuery("user", userInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "promoCode") {
      const offerObject = Parse.Object.extend("Offers");
      let offerInnerQuery = new Parse.Query(offerObject);
      offerInnerQuery.matches("offerCode", notesValue);
      ordersObj.matchesQuery("offerDetails", offerInnerQuery);
    } else if (notes !== "" && notes !== undefined && notes === "phoneNumber") {
      const shopObject = Parse.Object.extend("ShopLocations");
      let shopInnerpQuery = new Parse.Query(shopObject);
      shopInnerpQuery.equalTo("phoneNo", Number(notesValue));
      ordersObj.matchesQuery("shop", shopInnerpQuery);
    }else if (notes !== "" && notes !== undefined && notes === "total") {
      ordersObj.equalTo("totalCost", Number(notesValue));
    }    
    ordersObj.limit(1000000);

    ordersObj.find().then(result => {
        let dataList = [];
        let sno=0;        
        for (let i = 0; i < result.length; i++) {
          let shopPointer = "";
          let orderTime='';
          let email='';
          let userPointer='';
          let offerPoiter='';
          let locationName='';
          let businessPointer = "";
          let card='';
          let customerName='';
          let discountGiven='';
          let vat=0;
          let orderStatus='';
          let orderNotes='';
          let total=0;
          let subTotal=0;
          let time='';
          let promoCode='';
          let orderNo=result[i].id;
          let gateway='';
          let discountAmount=0;
          let customerNumber='';
          let shopNumber;
          orderStatus='';
          let refundTime = 0;
          let businessNumber='';
          let pikPercentageUser='';
          let commissionFee='';
          let isPaid = "false";
            if (result[i].get('createdAt') !== undefined && result[i].get('createdAt') !== "") { // current date and order date
              orderTime = date.dateFormat(result[i].get('createdAt'));
            } else {
              orderTime = "";
            }  
            if (result[i].get('isPaid') !== undefined && result[i].get('isPaid') !== "") { // current date and order date
              isPaid = result[i].get('isPaid').toString();
            }     
            if (result[i].get("owner") !== undefined && result[i].get("owner") !== "") {
              if (
                result[i].get("subTotal") !== undefined &&
                result[i].get("subTotal") !== ""
              ) {
                subTotal = Number(result[i].get("subTotal"));
              }
              businessPointer = result[i].get("owner"); // Business name
                
                if (
                    businessPointer.get("Business_name") !== undefined &&
                    businessPointer.get("Business_name") !== ""
                ) {
                    businessName = businessPointer.get("Business_name");
                } else {
                    businessName = "";
                }
                if (
                  businessPointer.get("phoneNumber") !== undefined &&
                  businessPointer.get("phoneNumber") !== ""
              ) {
                 businessNumber = businessPointer.get("phoneNumber");
               } else {
                 businessNumber = "";
              }
              if (
                businessPointer.get("pikPercentage") !== undefined &&
                businessPointer.get("pikPercentage") !== ""
              ) {
                pikPercentageUser = businessPointer.get("pikPercentage");
                commissionFee= (subTotal * pikPercentageUser)/100;
              } else {
                pikPercentageUser = "";
              }
            }
            if (result[i].get("shop") !== undefined && result[i].get("shop") !== "") {
                shopPointer = result[i].get("shop"); // Location name
                if (shopPointer.get("locationName") !== undefined &&
                  shopPointer.get("locationName") !== '') 
                  {
                    locationName = shopPointer.get("locationName");
                } else {
                    locationName = "No Shop";
                }
                if (
                  shopPointer.get("phoneNo") !== undefined &&
                  shopPointer.get("phoneNo") !== ""
                ) {
                  shopNumber = shopPointer.get("phoneNo");
                } else {
                  shopNumber = "";
                }
            } else {
                locationName = "No Shop";
            }
            if (result[i].get("email") !== undefined && result[i].get("email") !== "") {
              email = result[i].get("email");
            }
              if (
                result[i].get("cardLast4") !== undefined &&
                result[i].get("cardLast4") !== ""
              ) {
                card = result[i].get("cardLast4") ;
              } 
              if (result[i].get("user") !== undefined && 
                 result[i].get("user") !== "") {
                userPointer = result[i].get("user"); // user poiter class
                if (userPointer.get("name") !== undefined  &&
                userPointer.get("name") !== "" ) {
                  customerName = userPointer.get("name");
                } 
                if (userPointer.get("phoneNo") !== undefined  &&
                userPointer.get("phoneNo") !== "") {
                  customerNumber = userPointer.get("phoneNo");
                } 
              }
              if (
                result[i].get("discountPaidBy") !== undefined &&
                result[i].get("discountPaidBy") !== ""
              ) {
                discountGiven = result[i].get("discountPaidBy");
              } 
              if (
                result[i].get("discountAmount") !== undefined &&
                result[i].get("discountAmount") !== ""
              ) {
                discountAmount = result[i].get("discountAmount");
              } 
              if (result[i].get("notes") !== undefined && result[i].get("notes") !== "") {
                orderNotes = result[i].get("notes");
              }
              if (
                result[i].get("orderStatus") !== undefined &&
                result[i].get("orderStatus") !== ""
              ) {
                orderStatus = Number(result[i].get("orderStatus"));
                if(orderStatus===0){
                  refundTime = 0;
                  orderStatus="Not Started ";
                }else if(orderStatus===1){
                  refundTime = 1;
                  orderStatus="In Progress ";
                }else if(orderStatus===2){
                  refundTime = 1;
                  orderStatus="Ready For Pickup"
                }
              }
              if (result[i].get("tax") !== undefined && result[i].get("tax") !== "") {
                vat = Number(result[i].get("tax"));
              } 
              if (
                result[i].get("totalCost") !== undefined &&
                result[i].get("totalCost") !== ""
              ) {
                total = Number(result[i].get("totalCost"));
              } 
              
              if (
                result[i].get("totalTime") !== undefined &&
                result[i].get("totalTime") !== ""
              ) {
                time = Number(result[i].get("totalTime"));
              }
              if (
                result[i].get("offerDetails") !== undefined &&
                result[i].get("offerDetails") !== ""
              ) {
                offerPoiter = result[i].get("offerDetails"); // offerDetails pointer class
                if (
                  offerPoiter.get("offerCode") !== undefined ||
                  offerPoiter.get("offerCode") !== ""
                ) {
                  promoCode = offerPoiter.get("offerCode");
                }
              }
              if (
                result[i].get("gateway") !== undefined &&
                result[i].get("gateway") !== ""
              ) {
                gateway = result[i].get("gateway");
              }

          sno = sno + 1;
          const dataValue = {
            Sno: sno,
            orderTime: orderTime,
            customerName: customerName,
            shopName: businessName,
            shopLocation: locationName,
            shopNumber: shopNumber,
            customerNumber:customerNumber,
            businessNumber:businessNumber,
            email: email,
            discountGiven: discountGiven,
            card: card,
            orderNotes: orderNotes,
            orderStatus: orderStatus,
            refundTime: refundTime,
            vat: vat,
            subTotal: subTotal,
            time: time,
            total: total,
            promoCode: promoCode,
            orderNo: orderNo,
            isPaid: isPaid,
            gateway: gateway,
            discountAmount:discountAmount,
            commissionFee:commissionFee
          };
          dataList.push(dataValue);
        }
        this.setState({
          data: dataList,
          loading: false
        });
      },
      error => {
        log.error(error.message);
      }
    );
  }
  searchByBusiness(e) {
    const users = Parse.Object.extend("User");
    let usersQuery = new Parse.Query(users);
    if (e.target.value === "") {
        this.setState({
            selectedLocation: [],
            shopLocations: []
        });
        this.onload();
    }
    usersQuery.exists("Business_name");
    usersQuery.equalTo("Business_name", e.target.value);
    let businessObjId = [];
    usersQuery.find().then(
        result => {
            for (let i = 0; i < result.length; i++) {
                businessObjId.push(result[i].id);
            }
            // query for filter by date and country
            let countryCode = document.getElementById("country").value;
            const dateRange = document.getElementById("dateRange").value;
            let fromDate = date.fromDate(dateRange);
            let toDate = date.toDate(dateRange);
            const country = Parse.Object.extend("Country");
            let countryQuery = new Parse.Query(country);
            countryQuery.equalTo("shortName", countryCode);
            let countryObj = [];
            countryQuery.find().then(result => {
                for (let i = 0; i < result.length; i++) {
                    countryObj.push(result[i].id);
                }
                this.DataTable(countryObj, fromDate, toDate);
                this.filterByLocation(businessObjId);
            });
        },
        error => {
            log.error(error.message);
        }
    );
}

filterByLocation(businessObjId) {
    const shop = Parse.Object.extend("ShopLocations");
    let shopLocationsQuery = new Parse.Query(shop);
    shopLocationsQuery.containedIn("business", businessObjId);
    let shopLocations = [];
    shopLocationsQuery.find().then(
        result => {
            for (let i = 0; i < result.length; i++) {
                const dataValue = {
                    id: result[i].get("locationName"),
                    label: result[i].get("locationName")
                };
                shopLocations.push(dataValue);
            }
            this.setState({
                shopLocations: shopLocations
            });
        },
        error => {
            log.error(error.message);
        }
    );
}

  filterNotes(e) {
    document.getElementById("filterByNotes").disabled = false;
  }

  reset() {
    document.getElementById("notes").value = "";
    this.inputValue.value = "";
    document.getElementById("filterByNotes").disabled = true;
    this.onload();
  }

  search() {
    this.onload();
  }

  resetAll() {
    window.location.reload()
  }

  onSelect(e){
    let selectedLocation = [];
    for(let i in e){
        selectedLocation.push(e[i].id)
    } 
    this.setState({
        selectedLocation: selectedLocation
    })
}
onRemove(e){
  let selectedLocation = [];
  for(let i in e){
      selectedLocation.push(e[i].id)
  } 
  this.setState({
      selectedLocation: selectedLocation
  })
}

// for editing the table row
editRow(e, row) {
  let refundCost = row.refundTime
  let subTotal = row.subTotal;
  let refundPercentage = 4;
  let refundString = "";
  if(row.refundTime === 0){
    row.refundTime = "Before";
    refundPercentage = 4
    refundCost = (subTotal*4)/100;
    refundString = 4+"% of totalCost + 1 Aed" 
  } else {
    row.refundTime = "After";
    refundPercentage = 14;
    refundCost = (subTotal*14)/100;
    refundString = 14+"% of totalCost + 1 Aed";
  }
  this.setState({
    popUpData: [{
      objectId: row.orderNo,
      orderStatus: 0,
      subTotal: subTotal,
      refundTime: row.refundTime,
      refundCost: refundCost+1,
      refundTrans: 1,
      refundPercentage: refundPercentage,
      refundForBusiness: 0,
      refundPerBusiness: 0,
      refundString: refundString,
      cancelNote: "",
      cancelledBy: ""
    }]
  });
}

updateOrderStatus(e){
  if(e.target.value === "cancelled"){
    document.getElementById("cancelOrderField").hidden = false;
    const {
      popUpData
  } = this.state;
  popUpData[0].orderStatus = 4;
  this.setState(() => {
      return popUpData;
  });
  } else{
    document.getElementById("cancelOrderField").hidden = true;
    const {
      popUpData
  } = this.state;
  popUpData[0].orderStatus = 3;
  this.setState(() => {
      return popUpData;
  });
  }
}

updateCancelledBy(e){
  if(e.target.value === "user"){
    const {
      popUpData
  } = this.state;
  popUpData[0].refundForBusiness = (popUpData[0].subTotal*10)/100;
  popUpData[0].refundPerBusiness = 10;
  popUpData[0].cancelledBy = "user";
  this.setState(() => {
    return popUpData;
  });
} else{
  const {
    popUpData
  } = this.state;
  popUpData[0].refundForBusiness = 0;
  popUpData[0].refundPerBusiness = 0;
  popUpData[0].cancelledBy = "business";
  this.setState(() => {
      return popUpData;
  });
  }
}

handleRefundCost(e){
    const {
      popUpData
  } = this.state;
  let tempRefundCost = popUpData[0].refundCost;
  let tempRefundPercentage = popUpData[0].refundPercentage;
  popUpData[0].refundCost = (e.target.validity.valid) ? e.target.value : popUpData[0].refundCost;
  if(popUpData[0].subTotal<popUpData[0].refundCost){
    this.taostMessage("Refund Cost is more than Sub Total");
    popUpData[0].refundCost = tempRefundCost;
    popUpData[0].refundPercentage = tempRefundPercentage;
    return
  }
  if(Number(popUpData[0].refundCost) === 0){
    popUpData[0].refundPercentage = 0;
  } else {
    popUpData[0].refundPercentage = (100*Number(popUpData[0].refundCost))/popUpData[0].subTotal;
  }
  popUpData[0].refundTrans = 0;
  popUpData[0].refundString = popUpData[0].refundPercentage+"% of totalCost + 0 Aed";
  this.setState(() => {
      return popUpData;
  });
}

handleRefundPercentage(e){
    const {
      popUpData
  } = this.state;
  popUpData[0].refundPercentage = (e.target.validity.valid) ? e.target.value : popUpData[0].refundPercentage;
  popUpData[0].refundCost = (popUpData[0].subTotal*Number(popUpData[0].refundPercentage))/100;
  popUpData[0].refundTrans = 0;
  popUpData[0].refundString = popUpData[0].refundPercentage+"% of totalCost + 0 Aed";
  this.setState(() => {
      return popUpData;
  });
}

handleRefundTrans(e){
  const {
    popUpData
} = this.state;
let tempTrans = popUpData[0].refundTrans;
let tempRefundCost = popUpData[0].refundCost;
popUpData[0].refundTrans = (e.target.validity.valid) ? e.target.value : popUpData[0].refundTrans;
popUpData[0].refundCost = ((popUpData[0].subTotal*Number(popUpData[0].refundPercentage))/100)+Number(popUpData[0].refundTrans);
if(popUpData[0].subTotal<popUpData[0].refundCost){
    this.taostMessage("Refund Cost is more than Sub Total");
    popUpData[0].refundCost = tempRefundCost;
    popUpData[0].refundTrans = tempTrans;
    return
}
popUpData[0].refundString = popUpData[0].refundPercentage+"% of totalCost + " + popUpData[0].refundTrans+" Aed";
this.setState(() => {
    return popUpData;
});
}

handleRefundForBusiness(e){
    const {
      popUpData
  } = this.state;
  popUpData[0].refundForBusiness = (e.target.validity.valid) ? e.target.value : popUpData[0].refundForBusiness;
  if(Number(popUpData[0].refundForBusiness) === 0){
    popUpData[0].refundPerBusiness = 0;
  } else {
    popUpData[0].refundPerBusiness = (100*Number(popUpData[0].refundForBusiness))/popUpData[0].subTotal;
  }
  this.setState(() => {
      return popUpData;
  });
}

handleRefundPerBusiness(e){
    const {
      popUpData
  } = this.state;
  popUpData[0].refundPerBusiness = (e.target.validity.valid) ? e.target.value : popUpData[0].refundPerBusiness;
  popUpData[0].refundForBusiness = (popUpData[0].subTotal*Number(popUpData[0].refundPerBusiness))/100;
  this.setState(() => {
      return popUpData;
  });
}

handleCancelNote(e){
    const {
      popUpData
  } = this.state;
  popUpData[0].cancelNote = (e.target.validity.valid) ? e.target.value : popUpData[0].cancelNote;
  this.setState(() => {
      return popUpData;
  });
}

saveItem(){
  let orderStatus = this.state.popUpData[0].orderStatus;
  let orderId = this.state.popUpData[0].objectId;
  if(orderStatus !==3 && orderStatus !==4){
    this.taostMessage("First Select Order Status")
    return
  }
  if(orderStatus === 4 &&  this.state.popUpData[0].cancelledBy === ""){
    this.taostMessage("Select Cancelled By")
    return
  }
  this.setState({
    loading: true
  })
  $("#openOrder").modal("hide");
  const Orders = Parse.Object.extend("Orders");
  let ordersObj = new Parse.Query(Orders);
  ordersObj.equalTo("objectId", orderId);
  ordersObj.find().then(result => {
    const orderHistory = Parse.Object.extend("OrderHistory");
    const orderHistoryObj = new orderHistory();
    orderHistoryObj.save().then(async saveOrderHistoryObj => {
      let offerIdObj;
      let userdObj;
      let ownerObj;
      let shopObj;
      let cardObj;
      let deliveryTypeObj;
      let countryObj;
      if(result[0].get("offerDetails") !== undefined){
        let offerPointer = Parse.Object.extend("Offers");
        offerIdObj = offerPointer.createWithoutData(result[0].get("offerDetails").id);
      } if(result[0].get("user") !== undefined){
        let userPointer = Parse.Object.extend("_User");
        userdObj = userPointer.createWithoutData(result[0].get("user").id);
      } if(result[0].get("owner") !== undefined){
        let ownerPointer = Parse.Object.extend("_User");
        ownerObj = ownerPointer.createWithoutData(result[0].get("owner").id);
      } if(result[0].get("shop") !== undefined){
        let shopPointer = Parse.Object.extend("ShopLocations");
        shopObj = shopPointer.createWithoutData(result[0].get("shop").id);
      } if(result[0].get("cardUsed") !== undefined){
        let cardPointer = Parse.Object.extend("Cards");
        cardObj = cardPointer.createWithoutData(result[0].get("cardUsed").id);
      } if(result[0].get("deliveryType") !== undefined){
        let deliveryTypePointer = Parse.Object.extend("DeliveryType");
        deliveryTypeObj = deliveryTypePointer.createWithoutData(result[0].get("deliveryType").id);
      } if(result[0].get("country") !== undefined){
        let countryPointer = Parse.Object.extend("Country");
        countryObj = countryPointer.createWithoutData(result[0].get("country").id);
      }
      saveOrderHistoryObj.set("orderStatus", orderStatus);
      if(result[0].get("owner") !== undefined){
        saveOrderHistoryObj.set("business", result[0].get("owner").id);
      } if(result[0].get("notes") !== undefined){
        saveOrderHistoryObj.set("notes", result[0].get("notes"));
      } if(result[0].get("discountPaidBy") !== undefined){
        saveOrderHistoryObj.set("discountPaidBy", result[0].get("discountPaidBy"));
      } if(result[0].get("subTotal") !== undefined){
        saveOrderHistoryObj.set("subTotal", result[0].get("subTotal"));
      } if(result[0].get("gateway") !== undefined){
        saveOrderHistoryObj.set("gateway", result[0].get("gateway"));
      } if(result[0].get("offerEnabled") !== undefined){
        saveOrderHistoryObj.set("offerEnabled", result[0].get("offerEnabled"));
      } if(offerIdObj !== undefined){
        saveOrderHistoryObj.set("offerDetails", offerIdObj);
      } if(result[0].get("taxId") !== undefined){
        saveOrderHistoryObj.set("taxId", result[0].get("taxId"));
      } if(result[0].get("subTotalTax") !== undefined){
        saveOrderHistoryObj.set("subTotalTax", result[0].get("subTotalTax"));
      } if(result[0].get("shop") !== undefined){
        saveOrderHistoryObj.set("shopstring", result[0].get("shop").id);
      } if(result[0].get("countryString") !== undefined){
        saveOrderHistoryObj.set("countryString", result[0].get("countryString"));
      } if(result[0].get("tranRef") !== undefined){
        saveOrderHistoryObj.set("tranRef", result[0].get("tranRef"));
      } if(result[0].get("tax") !== undefined){
        saveOrderHistoryObj.set("tax", result[0].get("tax"));
      } if(userdObj !== undefined){
        saveOrderHistoryObj.set("user", userdObj);
      } if(result[0].get("owner").get("pikPercentage") !== undefined){
        saveOrderHistoryObj.set("pikPercentage", result[0].get("owner").get("pikPercentage"));
        if(orderStatus === 3){
          let pikCharges = (result[0].get("owner").get("pikPercentage")*result[0].get("subTotal"))/100
          orderHistoryObj.set("pikCharges", pikCharges);
        }
      }
       if(result[0].get("time") !== undefined){
        saveOrderHistoryObj.set("time", result[0].get("time"));
      } if(cardObj !== undefined){
        saveOrderHistoryObj.set("cardUsed", cardObj);
      } if(deliveryTypeObj !== undefined){
        saveOrderHistoryObj.set("deliveryType", deliveryTypeObj);
      } if(result[0].get("cardLast4") !== undefined){
        saveOrderHistoryObj.set("cardLast4", result[0].get("cardLast4"));
      } if(result[0].get("order") !== undefined){
        saveOrderHistoryObj.set("order", result[0].get("order"));
      }
      saveOrderHistoryObj.set("orderNo", orderId);
      if(result[0].get("currency") !== undefined){
        saveOrderHistoryObj.set("currency", result[0].get("currency"));
      } if(result[0].get("totalTime") !== undefined){
        saveOrderHistoryObj.set("totalTime", result[0].get("totalTime"));
      } if(result[0].get("totalCost") !== undefined){
        saveOrderHistoryObj.set("totalCost", result[0].get("totalCost"));
      } if(result[0].get("platform") !== undefined){
        saveOrderHistoryObj.set("platform", result[0].get("platform"));
      } if(ownerObj !== undefined){
        saveOrderHistoryObj.set("owner", ownerObj);
      } if(shopObj !== undefined){
        saveOrderHistoryObj.set("shop", shopObj);
      } if(result[0].get("isPaid") !== undefined){
        saveOrderHistoryObj.set("isPaid", result[0].get("isPaid"));
      } if(countryObj !== undefined){
        saveOrderHistoryObj.set("country", countryObj);
      } if(result[0].get("discountAmount") !== undefined){
        saveOrderHistoryObj.set("discountAmount", result[0].get("discountAmount"));
      } if(result[0].get("totalCostTax") !== undefined){
        saveOrderHistoryObj.set("totalCostTax", result[0].get("totalCostTax"));
      } if(result[0].get("totalCost") !== undefined){
        saveOrderHistoryObj.set("orderCost", result[0].get("totalCost"));
      } if(result[0].get("stripeReceipt_url") !== undefined){
        saveOrderHistoryObj.set("stripeReceipt_url", result[0].get("stripeReceipt_url"));
      }
      saveOrderHistoryObj.set("orderTime", result[0].get("createdAt"));
      if(result[0].get("stripeChargeId") !== undefined){
        saveOrderHistoryObj.set("stripeChargeId", result[0].get("stripeChargeId"));
      } if(result[0].get("email") !== undefined){
        saveOrderHistoryObj.set("email", result[0].get("email"));
      } if(result[0].get("user") !== undefined){
        saveOrderHistoryObj.set("userString", result[0].get("user").id);
      } if(result[0].get("pikFoodAddress") !== undefined){
        saveOrderHistoryObj.set("pikFoodAddress", result[0].get("pikFoodAddress").id);
      } if(result[0].get("userPlatform") === undefined){
        saveOrderHistoryObj.set("userPlatform", "CP");
      } else {
        saveOrderHistoryObj.set("userPlatform", result[0].get("userPlatform"));
      } if(result[0].get("pikFoodDeliveryType") !== undefined){
        saveOrderHistoryObj.set("pikFoodDeliveryType", result[0].get("pikFoodDeliveryType"));
      }
      saveOrderHistoryObj.set("orderVerified", 1);
      if(result[0].get("orderStatus") !== undefined){
        saveOrderHistoryObj.set("orderStatusBefore", result[0].get("orderStatus"));
      }
    if(orderStatus === 4){
      let refundTime = 0;
      if(this.state.popUpData[0].refundTime === "After"){
        refundTime = 1;
      }
      saveOrderHistoryObj.set("refundTime", refundTime);
      saveOrderHistoryObj.set("refundForBusiness", Number(this.state.popUpData[0].refundForBusiness));
      saveOrderHistoryObj.set("refundPerBusiness", Number(this.state.popUpData[0].refundPerBusiness));
      saveOrderHistoryObj.set("cancelledBy", this.state.popUpData[0].cancelledBy);
      saveOrderHistoryObj.set("refundCost", Number(this.state.popUpData[0].refundCost));
      saveOrderHistoryObj.set("refundString", this.state.popUpData[0].refundString);
      saveOrderHistoryObj.set("cancelNote", this.state.popUpData[0].cancelNote);
      saveOrderHistoryObj.set("refundTrans", Number(this.state.popUpData[0].refundTrans));
      saveOrderHistoryObj.set("refundPercentage", Number(this.state.popUpData[0].refundPercentage));
      orderHistoryObj.set("pikCharges", (Number(this.state.popUpData[0].refundCost)-Number(this.state.popUpData[0].refundForBusiness)));
    }
      await saveOrderHistoryObj.save();
      this.deleteOrderFromOrderClass(orderId);
    })
  })
}

deleteOrderFromOrderClass(orderId){
  const Orders = Parse.Object.extend("Orders");
  let ordersObj = new Parse.Query(Orders);
  ordersObj.equalTo("objectId", orderId);
  ordersObj.find().then(result => {
    Parse.Object.destroyAll(result);
    swal("Great", "Data Updated successfully", "success");
    this.setState({
      popUpData: []
    })
    $("#dropDown")[0].selectedIndex = 0;
    document.getElementById("cancelOrderField").hidden = true;
    this.onload();
  })
}

cancel(){
  this.setState({
    popUpData: []
  })
  $("#dropDown")[0].selectedIndex = 0;
  document.getElementById("cancelOrderField").hidden = true;
}

editIsPaid(e, row){
  this.setState({
    loading: true
  })
  let isPaid = false;
  if(e.target.value === "true"){
    isPaid = true;
  }
    const Orders = Parse.Object.extend("Orders");
    let ordersObj = new Parse.Query(Orders);
    ordersObj.equalTo("objectId", row.orderNo);
    ordersObj.find().then(result => {
      ordersObj.first().then(async orderData => {
        orderData.set("isPaid", isPaid)
        await orderData.save();
        this.onload()
      })
    })
  }

  render() {
    const columns = [
      {
        Header: "Edit",
        accessor: "edit",
        Cell: ({ row }) => (
          <button
            className="editButton"
            id="editRow"
            data-toggle="modal"
            data-target="#openOrder"
            onClick={e => this.editRow(e, row)}
          >
            edit
          </button>
        )
      },
      {
        Header: "S.no",
        accessor: "Sno"
      },
      {
        Header: "Date",
        accessor: "orderTime"
      },
      {
        Header: "Order Status",
        accessor: "orderStatus"
      },
      {
        Header: "Refund Time",
        accessor: "refundTime",
        show : false
      },
      {
        Header: "Name",
        accessor: "customerName"
      },
      {
        Header: "Shop Name",
        accessor: "shopName"
      },
      {
        Header: "Shop Location",
        accessor: "shopLocation"
      },
      {
        Header: "Order ID",
        accessor: "orderNo"
      },
      {
        Header: "Is Paid",
        accessor: "isPaid",
        Cell: ({ row }) => (
          <select
            style={{height:"2em", width:"100%"}}
            id="isPaid"
            onChange={e => this.editIsPaid(e, row)}
          >
            <option value="" hidden>{row.isPaid}</option>
            <option value="true">True</option>
            <option value="false">False</option>
          </select>
        )
      },
      {
        Header: "Time",
        accessor: "time"
      },
      {
        Header: "Sub Total",
        accessor: "subTotal"
      },
      {
        Header: "Total",
        accessor: "total"
      },
      {
        Header: "Vat",
        accessor: "vat"
      },
      {
        Header: "Order Notes",
        accessor: "orderNotes"
      },
      {
        Header: "Order Time",
        accessor: "orderTime"
      },
      {
        Header: "Discount Given",
        accessor: "discountGiven"
      },
      {
        Header: "Discount Amount",
        accessor: "discountAmount"
      },
      {
        Header: "Business Ph Number",
        accessor: "businessNumber"
      },
      {
        Header: "Shop Ph Number",
        accessor: "shopNumber"
      },
      {
        Header: "Customer Ph Number",
        accessor: "customerNumber"
      },
      {
        Header: "Email",
        accessor: "email"
      },
      {
        Header: "Card",
        accessor: "card"
      },
      {
        Header: "Promo Code",
        accessor: "promoCode"
      },
      {
        Header: "Payment Gateways",
        accessor: "gateway"
      }
    ];
    const { loading } = this.state;
    return (
      <MainLayout>
        <main role="main" className=" flex-grow-2  container-p-y">
          <div className="container">
            <div className="row">
              <div className="col-4">
                <PredefinedRanges id="dateRange" />
              </div>
              <div className="col-2">
                <select
                  className="custom-select"
                  id="orderStatus"
                >
                  <option value="" hidden>Order Types</option>
                  <option value="notStarted" id="notStarted">
                    Not Started
                  </option>
                  <option value="inProgress" id="inProgress">
                    In Progress
                  </option>
                  <option value="readyForPickup" id="readyForPickup">
                    Ready For Pickup
                  </option>
                  <option value="" id="">
                    All (Orders)
                  </option>
                </select>
              </div>
              <div className="col-2">
                <select
                  className="custom-select"
                  id="platform"
                >
                  <option value="" hidden>Platform</option>
                  <option value="PF" id="PF">
                    Pik Food
                  </option>
                  <option value="CP" id="CP">
                    Coffee Pik
                  </option>
                  <option value="BN" id="BN">
                    Beans
                  </option>
                  <option value="" id="">
                    All (Platform)
                  </option>
                </select>
              </div>
              <div className="col-4">
                <select
                  className="filter-note"
                  id="notes"
                  onChange={e => this.filterNotes(e)}
                >
                  <option value="">Notes</option>
                  <option value="name" id="name">
                    Name
                  </option>
                  <option value="phoneNumber" id="phoneNumber">
                    Phone Number
                  </option>
                  <option value="card" id="card">
                    Card
                  </option>
                  <option value="orderId" id="orderId">
                    Order ID
                  </option>
                  <option value="promoCode" id="promoCode">
                    Promo Code
                  </option>
                  <option value="gateWays" id="gateWays">
                    Gateways
                  </option>
                  <option value="total" id="total">
                    Total
                  </option>
                </select>
                <input
                  type="text"
                  id="filterByNotes"
                  className="filter-note-text"
                  placeholder="&nbsp; Filter by notes"
                  ref={el => (this.inputValue = el)}
                  disabled
                />
                <input
                  type="text"
                  id="input2"
                  size="1"
                  className="filter-note-iconBox notes-text-icon pointer"
                  onClick={() => this.reset()}
                />
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-lg-4">
                <input className="search__input" id="searchForBusiness" onKeyUp={(e) => this.searchByBusiness(e)} placeholder="search" />
              </div>
              <div className="col-lg-4">
                <Multiselect
                  options={this.state.shopLocations}
                  displayValue="id"
                  onSelect={(e)=>this.onSelect(e)}
                  onRemove={(e)=>this.onRemove(e)}
                  placeholder="Select Location"
                  showCheckbox={false}
                  avoidHighlightFirstOption={true}
                  closeOnSelect={false}
                  style={{ chips: { background: "#673ab7" }, multiselectContainer: {color: "#673ab7"}}}
                />
              </div>
              <div className="col-lg-2">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.search()}
                >
                  Search
                </button>
              </div>
              <div className="col-lg-2">
                <button
                  type="button"
                  className="searchBtn"
                  onClick={() => this.resetAll()}
                >
                  Reset
                </button>
              </div>
            </div>
            <br></br>
            <div className="row">
              <div className="col-12 text-center cardPending">
                <h4>
                  <CountUp end={Number(this.state.totalOrders)} />
                  {this.state.currency}
                </h4>
                <p>TOTAL ORDERS</p>
              </div>
            </div>
            <br />
            <Loader loading={loading} />
            <ReactTable
              data={this.state.data}
              columns={columns}
              defaultPageSize={10}
              pageSizeOptions={[3, 5, 10, 15, 50, 100]}
            />
          </div>
           {/* popup for Editing Order starts here */}
          <div className="modal fade" id="openOrder" tabIndex="-1" role="dialog" aria-labelledby="openMenuTitle" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered modal-items" role="document">
                  <div className="modal-content">
                      <div className="modal-header" style={{backgroundColor: "#673ab7" }}>
                          <h4 className="modal-title" style={{color: "#ffffff"}}>Edit Order</h4>
                          <button type="button" className="closePopUp" data-dismiss="modal" onClick={()=>this.cancel()}><b>&times;</b></button>
                      </div>
                      <div className="modal-body">
                        <div className="row">
                          <div className="col-12">
                            <select className="custom-select" id="dropDown" onChange={(e)=> this.updateOrderStatus(e)}>
                              <option value="">---- Update Order Status ----</option>
                              <option value="completed">Completed</option>
                              <option value="cancelled">Cancelled</option>
                            </select>
                          </div>
                        </div><br />
                        <div id="cancelOrderField" hidden>
                          {this.state.popUpData.map((o, index) => (
                            <div key={index}>
                              <div className="row">
                                <div className="col-6">Business Accept</div>
                                <div className="col-6">Refund Cost</div>
                              </div>
                              <div className="row">
                                <div className="col-6"><input type="text" value={o.refundTime} className="form-control" disabled /></div>
                                <div className="col-6"><input type="text" pattern="[/^\d{3}\.\d{2}$/]*" value={o.refundCost} onChange={(e)=> this.handleRefundCost(e)} className="form-control" /></div>
                              </div>
                              <div className="row">
                                <div className="col-6">Refund Trans</div>
                                <div className="col-6">Refund Percentage</div>
                              </div>
                              <div className="row">
                                <div className="col-6"><input type="text" pattern="[/^\d{3}\.\d{2}$/]*" value={o.refundTrans} onChange={(e)=> this.handleRefundTrans(e)} className="form-control" /></div>
                                <div className="col-6"><input type="text" pattern="[/^\d{3}\.\d{2}$/]*" value={o.refundPercentage} onChange={(e)=> this.handleRefundPercentage(e)} className="form-control" /></div>
                              </div>
                              <div className="row">
                                <div className="col-6">Refund String</div>
                                <div className="col-6"> Sub Total</div>
                              </div>
                              <div className="row">
                                <div className="col-6"><input type="text" value={o.refundString} className="form-control" disabled /></div>
                                <div className="col-6"><input type="text" value={o.subTotal} className="form-control" disabled /></div>
                              </div>
                              <div className="row">
                                <div className="col-6">Refund For Business</div>
                                <div className="col-6">Refund Per Business</div>
                              </div>
                              <div className="row">
                                <div className="col-6"><input type="text" pattern="[/^\d{3}\.\d{2}$/]*" value={o.refundForBusiness} onChange={(e)=> this.handleRefundForBusiness(e)} className="form-control" /></div>
                                <div className="col-6"><input type="text" pattern="[/^\d{3}\.\d{2}$/]*" value={o.refundPerBusiness} onChange={(e)=> this.handleRefundPerBusiness(e)} className="form-control" /></div>
                              </div>
                              <div className="row">
                                <div className="col-6">CancelledBy*</div>
                                <div className="col-6">Cancel Note</div>
                              </div>
                              <div className="row">
                                <div className="col-6">
                                  <select className="custom-select" onChange={(e)=> this.updateCancelledBy(e)}>
                                    <option>---- Select CancelledBy ----</option>
                                    <option value="user">User</option>
                                    <option value="business">Business</option>
                                  </select>
                                </div>
                                <div className="col-6"><input type="text" value={o.cancelNote} onChange={(e)=> this.handleCancelNote(e)} className="form-control" /></div>
                            </div>
                          </div>))}
                        </div>
                      </div>
                      <div className="modal-footer">
                          <div className="container">
                              <div className="row">
                                  <div className="col-8 text-right">
                                      <button type="button" id="closeEditOrder" data-dismiss="modal" onClick={()=>this.cancel()}>Cancel</button>
                                  </div>
                                  <div className="col-4">
                                      <button type="button" id="saveEditOrder" className="pull-right" onClick={()=>this.saveItem()}>save</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          {/* popup for Editing Order Ends Here */}
        </main>
      </MainLayout>
    );
  }
}

export default CompletedOrders;
