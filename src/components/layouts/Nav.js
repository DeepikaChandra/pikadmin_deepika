import React, {Component} from "react";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/actions';
import '../../assets/css/navElements.css';
import { Link } from 'react-router-dom';

class Nav extends Component {
  logout = () => {
    this.props.onLogout();
  };

  render() {
    return (
<nav className="navbar navbar-expand-lg navbar-expand-sm navbar-expand-xs navbar-expand-md navbar-dark bg-dark sticky-top">
	<div className="navbar-collapse flex-column " id="navbar">
		<ul className="navbar-nav  w-100 navbar-custom px-3 pull-right">
			<div className="navbar-collapse mt-2" id="navbarCollapse">
				<h3 className="text-color col-lg col-md col-sm col-xs">Admin</h3>
				<div className="navbar-nav ml-auto">
					<div className="row">
						<div className="col-lg col-md col-sm col-xs">
							<form>
								<input type="text" size="30" className="text-white search-box placeholder" placeholder=" Search" />
							</form>
						</div>
						<div className="col-lg col-md col-sm col-xs">
							<label>
								<select id="country" className="countryCard specialColor">
									<option value="AE" id="uae">UAE</option>
									<option value="US" id="us">USA</option>
									<option value="KSA" id="ksa">KSA</option>
								</select>
							</label>
						</div>
						<div className="col-lg col-md col-sm col-xs">
							<label className="pointer">
								<img src={require( "../../assets/images/bell.png")} width="20" height="20" alt="-" />
							</label>
						</div>
						<div className="col-lg col-md col-sm col-xs">
							<a  href="/#" className="pointer" data-toggle="dropdown">
								<img src={require( "../../assets/images/settings.png")} width="20" height="20" alt="-" />
							</a>
							<div className="dropdown-menu dropdown-menu-right">
								<Link to="/profile" id="settings" className="dropdown-item">Profile</Link>
								<Link to="/reset" id="settings" className="dropdown-item">Reset Password</Link>
							</div>
						</div>
						<div className="col-lg col-md col-sm col-xs">
							<label onClick={()=>this.logout()} className="pointer" >
								<img src={require( "../../assets/images/logout.png")} width="20" height="20" alt="-" />
							</label>
						</div>
					</div>
				</div>
			</div>
		</ul>
	</div>
</nav> 
    );
  }
}



const mapStateToProps = state => {
  return {
    fullName: state.userReducer.fullName,
    email: state.userReducer.email
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(actions.logout())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);