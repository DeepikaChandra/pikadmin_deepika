import React from "react";
import SideNav from './Sidenav';
import Nav from './Nav';
import { ToastContainer } from "react-toastify";

const mainLayout = props => (
<div>
    <div>
        <Nav />
        <div>
            <SideNav />
            <div className="content">
                {props.children}
            </div>
        </div>
    </div>
    <ToastContainer autoClose={3000} />
</div>
);

export default mainLayout;