import React, {Component} from "react";
import { NavLink } from 'react-router-dom';

class SideNav extends Component {

  render() {
    return (
        <div className="row" id="body-row">
        <div id="sidebar-container" className="sidebar-expanded d-none d-md-block">
            <ul className="list-group">
                <NavLink to="/totalUser" className="list-group-item list-group-item-action">
                    <div>
                    <span className="fas fa-users fa-fw mr-3"></span>
                        <span>Total user</span>
                        <span className="submenu-icon ml-auto"></span>
                    </div>
                </NavLink>
                <NavLink to="/businessUser" className="list-group-item list-group-item-action">
                    <div>
                        <span className="fas fa-user fa-fw mr-3"></span>
                        <span>Business User</span>
                        <span className="submenu-icon ml-auto"></span>
                    </div>
                </NavLink>
                <NavLink to="/pushNotification" className="list-group-item list-group-item-action">
                    <div>
                        <span className="fas fa-bell fa-fw mr-3"></span> 
                        <span>Push Notification</span>
                        <span className="submenu-icon ml-auto"></span>
                    </div>
                </NavLink>
                <NavLink to="/reports" className="list-group-item list-group-item-action">
                    <div>
                        <span className="fas fa-file fa-fw mr-3"></span> 
                        <span>Reports</span>
                        <span className="submenu-icon ml-auto"></span>
                    </div>
                </NavLink>
                <a href="#submenu2" data-toggle="collapse" aria-expanded="false" className=" list-group-item list-group-item-action flex-column align-items-start">
                    <div className="d-flex w-100 justify-content-start align-items-center">
                        <span className="fas fa-box fa-fw mr-3"></span>
                        <span className="menu-collapsed">Orders</span>
                        <span className="submenu-icon ml-auto"></span>
                    </div>
                </a>
                <div id='submenu2' className="collapse sidebar-submenu">
                    <NavLink to="pendingOrders" className="list-group-item list-group-item-action">
                        <span className="fas fa-clock fa-fw mr-3"></span> 
                        <span>Pending Orders</span>
                    </NavLink>
                    <NavLink to="completedOrders" className="list-group-item list-group-item-action">
                        <span className="fas fa-check-circle fa-fw mr-3"></span>
                        <span>Completed Orders</span>
                    </NavLink>
                </div>
                <NavLink to="/marketingSales" className="list-group-item list-group-item-action">
                    <div>
                        <span className="fas fa-chart-line fa-fw mr-3"></span> 
                        <span>Marketing Sales</span>
                        <span className="submenu-icon ml-auto"></span>
                    </div>
                </NavLink>
            </ul>
            <p style={{position: "absolute", fontSize: "12px", bottom: "40px", left:"20%"}}>version 1.0.0.4</p>
        </div>
    </div>
    );
  }
}

export default SideNav;