import React from "react";
import { ToastContainer } from "react-toastify";
import BgImg from '../../assets/images/coffee.jpg';

const loginLayout = props => (
  <div>
    <div className="page-loader">
      <div className="bg-primary"></div>
    </div>

    {/* <!-- Content-- > */}

    <div className="authentication-wrapper authentication-3">
      <div className="authentication-inner">

        {/* <!-- Side container -->
        <!-- Do not display the container on extra small, small and medium screens --> */}
        <div className="d-none d-lg-flex col-lg-8 align-items-center p-5" style={{
          "backgroundColor": "#673ab7"
        }}>
          <div className="ui-bg-overlay bg-dark opacity-50"></div>

          {/* <!-- Text --> */}
          <div className="w-100 text-white text-center">
            <h1 className="display-3 font-weight-bolder"><b>Pik</b></h1>
            <div className="text-large font-weight-light">
              <b>Admin Panel</b>
            </div>
          </div>
          {/* <!-- /.Text --> */}
        </div>
        {/* <!-- / Side container --> */}
        {/* <!-- Form container --> */}
        <div className="d-flex col-lg-4 bg-white align-items-center ui-bg-cover ui-bg-overlay-container p-5" style={{
          "backgroundImage": "url(" + BgImg + ")",
          "backgroundSize": "cover"
        }}>
          {/* <!-- Inner container -->
          <!-- Have to add `.d-flex` to control width via `.col-*` classNamees --> */}
          <div className="d-flex col-sm-7 col-md-5 col-lg-12 px-0 px-xl-4 mx-auto">
            <div className="w-100">
              {/* <!-- Logo -->
              <div className="d-flex justify-content-center align-items-center">
                <div className="ui-w-60">
                  <div className="w-100 position-relative" style="padding-bottom: 54%">

                  </div>
                </div>
              </div>
               <!-- / Logo --> */}
              {/* <!-- Form --> */}
              {props.children}
              {/* <!-- / Form --> */}

            </div>
          </div>
        </div>
        {/* <!-- / Form container --> */}
        <ToastContainer autoClose={3000} />
      </div >
    </div >

    {/* <!-- / Content --> */}

    {/* <!-- Libs --> */}
  </div >
);

export default loginLayout;