import React from "react";
import { ToastContainer } from "react-toastify";

const authLayout = props => (
  <div>
    <div className="page-loader">
      <div className="bg-primary"></div>
    </div>
    {/* <!-- Content-- > */}
    <div className="authentication-wrapper authentication-2 px-4">
      <div className="authentication-inner py-5">
        {props.children}
      </div>
      <ToastContainer autoClose={3000} />
    </div>
    {/* <!-- Libs --> */}
    <script src="static/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
  </div >
);

export default authLayout;