import React, { Component } from 'react';

class Form extends Component {
  
  state = {};

  validate = () => {

    const formEl = this.formEl;
    const formLength = formEl.length;

    if (formEl.checkValidity() === false) {
      for (let i = 0; i < formLength; i++) {
        const elem = formEl[i];
        const errorLabel = elem.parentNode.querySelector(".invalid-feedback");
        if (errorLabel && elem.nodeName.toLowerCase() !== "button") {
          if (!elem.validity.valid) {
            errorLabel.textContent = elem.validationMessage;
          } else {
            errorLabel.textContent = "";
          }
        }
      }
      this.setState({className: 'was-validated'});
      return false;
    } else {
      //The form is valid, so we clear all the error messages
      for (let i = 0; i < formLength; i++) {
        const elem = formEl[i];
        const errorLabel = elem.parentNode.querySelector(".invalid-feedback");
        if (errorLabel && elem.nodeName.toLowerCase() !== "button") {
          errorLabel.textContent = "";
        }
      }
      return true;
    }
  };

  reset = () => {
    const formEl = this.formEl;
    const formLength = formEl.length;
    for (let i = 0; i < formLength; i++) {
      const elem = formEl[i];
      const errorLabel = elem.parentNode.querySelector(".invalid-feedback");
      if (errorLabel && elem.nodeName.toLowerCase() !== "button") {
        errorLabel.textContent = "";
      }
    }

  }

  submitHandler = (event) => {
    event.preventDefault();
    if (this.validate()) {
      this.props.onSubmit(event);
    }
  };

  render() {
    const props = { ...this.props };
    let classNames = [];
    classNames.push(props.className);
    if (this.state.className) {
      classNames.push(this.state.className);
      delete this.state.className;
    }
    //The form will have a refference in the component and a submit handler set to the component's submitHandler
    return (
      <form
        {...props}
        className={classNames.join(' ')}
        noValidate
        submit={this.props.submit}
        ref={form => (this.formEl = form)}
        onSubmit={this.submitHandler}
        onChange={this.reset}
        onClick={this.reset}>
        {this.props.children}
      </form>
    );
  }
}

export default Form;