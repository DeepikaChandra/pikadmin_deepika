import React, { Component } from "react";

class Pagination extends Component {

  componentWillMount() {
    this.props.onPageChanged(1);
  }

  handleClick = (page, e) => {
    e.preventDefault();
    this.props.onPageChanged(page);
  };

  render() {
    const pages = [];
    const totalPages = this.props.records.total_pages;
    for(let i=1 ; i<= totalPages; i++) {
      pages.push(i);
    }
    return (
      <nav>
        <ul className="pagination pagination-sm">
          <li className="page-item">
            <button className="page-link" onClick={e => this.handleClick(1, e)}>
              {"<<"}
            </button>
          </li>
          <li className="page-item">
            <button className="page-link" disabled={!this.props.records.previous_page}  onClick={e => this.handleClick(this.props.records.previous_page, e)}>
              {"<"}
            </button>
          </li>
          {pages.map((page, index) => {
            return (
              <li key={index} className={`page-item${this.props.records.current_page === page ? " active" : ""}`}>
                <button className="page-link"  onClick={e => this.handleClick(page, e)}>
                  {page}
                </button>
              </li>
            );
          })}
          <li className="page-item">
            <button className="page-link" disabled={!this.props.records.next_page} onClick={e => this.handleClick(this.props.records.next_page, e)}>
              {">"}
            </button>
          </li>
          <li className="page-item">
            <button className="page-link"  onClick={e => this.handleClick(this.props.records.total_pages, e)}>
              {">>"}
            </button>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Pagination;
