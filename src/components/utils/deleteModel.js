import React from 'react';
import Modal from 'react-responsive-modal';

const model = props => (
    <Modal open={props.open} onClose={props.close} center>
        <div className="model-content">
            <div className="modal-body">
                <h5>Are you sure? <br /> <br />
                    Do You want to delete {props.name} ??</h5>
            </div>
            <div className="model-footer pull-right">
                <button type="button" onClick={props.close} className="btn btn-warning mx-2">Cancel</button>
                <button type="button" onClick={props.delete} className="btn btn-danger mx-2">Delete</button>
            </div>
        </div>
    </Modal>
);

export default model;