
import moment from 'moment';

export const dateTimeFormat = (date) => {
    var momentDate = moment(date, 'YYYY-MM-DD');
    var dateFormat = momentDate.toDate();
    return dateFormat;
  }